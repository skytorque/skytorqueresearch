# SkyTorque Research

This is the public repository for research on the SkyTorque software form SkyEcho. The SkyTorque radar software processes radar voltage data in realtime into useful radar data prodcuts.

## Documentation 

[SkyTorque radar data formats](docs/SkyTorque_radar_data_formats_and_filenames_v2.0.pdf)

[SkyTorque level1 format](docs/sl1_format.pdf)

## Code library

Some essential python code to use SkyTorque data can be found in the [code library](lib/).

[A python script to read sl1 data](lib/level1_io.py)

## Public tools for processing weather radar data.

[Wradlib](https://docs.wradlib.org/en/stable/)

[Py-ART](https://arm-doe.github.io/pyart/)

[pySTEPS](https://pysteps.github.io/)

## Presentation slides

Presentation slides form the SkyTorque research meetings that are shared can be found in this folder.

[Slides](slides/)

## Authors

Albert Oude Nijhuis, SkyEcho.

## Acknowledgments

TBD
