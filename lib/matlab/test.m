% Example how to load sl1 data.
% - quick example of a power spectrum plot.
% - no spectral processing (no filtering, no windowing to remove aliasing)

% set filename and time index.
test_sl1_filename = "/Users/albertoudenijhuis/Downloads/data/RIJNMOND_rawdata/2020-10-06_RIJNMOND_rawdata_rain/level1_data/level1_data_RIJNMOND_2020-10-06T11-21-19Z_000000001.sl1";
time_index = 2

% load the data
mcl1_data = load_mcl1_data_from_sl1(test_sl1_filename, time_index);

% apply the fft
% - do fftshift
% - remove fft samples factor
data_HH_fft2_scaled = fftshift(fft2(...
    mcl1_data.data_HH, ...
    mcl1_data.n_dopfreq, ...
    mcl1_data.n_samples) / (mcl1_data.n_samples * mcl1_data.n_dopfreq));

% remove negative ranges (mirrored data)
data_HH_fft2_scaled = data_HH_fft2_scaled(:,1:mcl1_data.n_range);

% quick power spectrum calculation
% - first in linear values
% - multiply with radar constant
% then calculate dBZ values

pow_spectrum_lin_HH = (...
     abs(data_HH_fft2_scaled .^2) ...
     .* mcl1_data.radar_constant_lin_HH);
pow_spectrum_dB_HH = pow2db(pow_spectrum_lin_HH);

% plot it.
imagesc(...
    mcl1_data.doppler_velocity_ms, ...
    1.e-3 * mcl1_data.range_m, ...
    pow_spectrum_dB_HH.');
ax = gca;
ax.YDir = 'normal'

xlabel('Doppler velocity [m s^{-1}]') 
ylabel('Range [km]') 

title('Doppler spectrum HH')

mycb = colorbar
caxis([-30, 50]);
set(get(mycb,'label'),'string','Reflectivity [dBZ]');