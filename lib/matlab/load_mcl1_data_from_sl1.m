% load_mcl1_data_from_sl1.m
% - A matlab script from SkyEcho.
% - Read some data from a sl1 radar data file.
% 
% Copyright 2021 SkyEcho
%
% Update history
% - First version (AON, 2021-03-12).
%
% wishlist:
% - using a sweep buffer, to give versatility in selecting the number of
%   doppler frequencies that is used in the processing.
%
% note:
% - the python script is more advanced

function mcl1_data = load_mcl1_data_from_sl1(...
        sl1_filename, time_index)
    % Convenient function to load mcl1 data using indexing.    
    mcl1_data = struct;
    
    % check if file exists.
    if ~isfile(sl1_filename)
        disp('File does not exist.');
        return        
    end
    
    sl1_f = fopen(sl1_filename,'r');
    
    % read file header attributes
    attributes = sl1_read_file_header(sl1_f);
    
    % skipping
    for i_skip = 1:time_index
        % read header
        sl1_block_header = sl1_read_block_header(sl1_f, attributes);
        
        % in case of EOF, try to open a new file
        if feof(sl1_f)
            next_sl1_filename = get_next_sl1_filename(sl1_filename);
            
            if isfile(next_sl1_filename) 
                % next file exists, go for it.
                sl1_filename = next_sl1_filename;
                fclose(sl1_f);
                sl1_f = fopen(sl1_filename,'r');
                attributes = sl1_read_file_header(sl1_f);
                sl1_block_header = sl1_read_block_header(sl1_f, attributes);
            else 
                % eof, and no next file
                display("Data does not exists for this time index.")
                return
            end
        end        
        
        % skip data
        new_pos = ftell(sl1_f) + sl1_block_header.n_bytes_block_data;
        fseek(sl1_f, new_pos ,'bof');
    end    

    % read header
    sl1_block_header = sl1_read_block_header(sl1_f, attributes);
    
    % read data
    sl1_block_data = sl1_read_block_data(...
        sl1_f, attributes, sl1_block_header);
    
    fclose(sl1_f);
    
    % calculate extra inf
    extra_inf = calc_extra_inf(attributes, sl1_block_header);
    
    % merge structs for final result
    names = [fieldnames(attributes); fieldnames(sl1_block_header); fieldnames(sl1_block_data); fieldnames(extra_inf)];
    mcl1_data = cell2struct([struct2cell(attributes); struct2cell(sl1_block_header); struct2cell(sl1_block_data); struct2cell(extra_inf)], names, 1);
end

function attributes = sl1_read_file_header(sl1_f)
    % Read an sl1 file header.
    attributes = struct;

    % file identifier
    binary_word = char(fread(sl1_f, [1,3], '3*uchar'));
    sl1_file_identifier_check = isequal(...
        binary_word,...
        'sbf');
    if (sl1_file_identifier_check == false)
        disp('Failed file identifier check.');
        return
    end
    
    % version number and header length
    version_number_major = fread(sl1_f, 1, 'int32');
    version_number_minor = fread(sl1_f, 1, 'int32');
    header_length = fread(sl1_f, 1, 'int32');
    
    sl1_file_version_check = isequal(...
        version_number_major, 1) & ...
        isequal(...
        version_number_minor, 0);
    if (sl1_file_version_check == false)
        disp('Failed version check.');
        return
    end
    
    % n_samples, n_dopfreq
    attributes.n_samples = fread(sl1_f, 1, 'int32');
    attributes.n_dopfreq = fread(sl1_f, 1, 'int32');   

    % valid_channel_tags, data_is_complex
    HH_valid_channel_tag = fread(sl1_f, 1, 'uint8');
    HV_valid_channel_tag = fread(sl1_f, 1, 'uint8');
    VH_valid_channel_tag = fread(sl1_f, 1, 'uint8');
    VV_valid_channel_tag = fread(sl1_f, 1, 'uint8');
    attributes.data_is_complex = isequal(fread(sl1_f, 1, 'uint8'), 1);
    
    valid_channel_tags = [];
    if HH_valid_channel_tag
        valid_channel_tags = [valid_channel_tags, "HH"];
    end
    if HV_valid_channel_tag
        valid_channel_tags = [valid_channel_tags, "HV"];
    end
    if VH_valid_channel_tag
        valid_channel_tags = [valid_channel_tags, "VH"];
    end
    if VV_valid_channel_tag
        valid_channel_tags = [valid_channel_tags, "VV"];
    end    
    attributes.valid_channel_tags = valid_channel_tags;

    % radar_constant_factor_dB_*
    attributes.radar_constant_factor_dB_HH = fread(sl1_f, 1, 'float32');
    attributes.radar_constant_factor_dB_HV = fread(sl1_f, 1, 'float32');
    attributes.radar_constant_factor_dB_VH = fread(sl1_f, 1, 'float32');
    attributes.radar_constant_factor_dB_VV = fread(sl1_f, 1, 'float32');

    % calibration_offset_dB_*
    attributes.calibration_offset_dB_HH = fread(sl1_f, 1, 'float32');
    attributes.calibration_offset_dB_HV = fread(sl1_f, 1, 'float32');
    attributes.calibration_offset_dB_VH = fread(sl1_f, 1, 'float32');
    attributes.calibration_offset_dB_VV = fread(sl1_f, 1, 'float32');
    
    % max_abs_value_*
    attributes.max_abs_value_HH = fread(sl1_f, 1, 'float32');
    attributes.max_abs_value_HV = fread(sl1_f, 1, 'float32');
    attributes.max_abs_value_VH = fread(sl1_f, 1, 'float32');
    attributes.max_abs_value_VV = fread(sl1_f, 1, 'float32');    

    % sampling_frequency_MHz
    attributes.sampling_frequency_MHz = fread(sl1_f, 1, 'float32');    
        
    % antenna_mode
    attributes.antenna_mode = fread(sl1_f, 1, 'int32');    
    
    % bandwidth_MHz
    % central_frequency_GHz
    % sweep_time_mus
    % dual_transmission
    attributes.bandwidth_MHz = fread(sl1_f, 1, 'float32');    
    attributes.central_frequency_GHz = fread(sl1_f, 1, 'float32');        
    attributes.sweep_time_mus = fread(sl1_f, 1, 'float32');        
    attributes.dual_transmission = isequal(fread(sl1_f, 1, 'uint8'), 1);

    % altitude_m
    % latitude_deg
    % longitude_deg    
    attributes.altitude_m = fread(sl1_f, 1, 'float32');        
    attributes.latitude_deg = fread(sl1_f, 1, 'float64');        
    attributes.longitude_deg = fread(sl1_f, 1, 'float64');        
    
    attributes.instrument_name = deblank(char(fread(sl1_f, [1,100], '100*uchar')));
    attributes.institution = deblank(char(fread(sl1_f, [1,100], '100*uchar')));
    attributes.references = deblank(char(fread(sl1_f, [1,100], '100*uchar')));
    attributes.e_mail = deblank(char(fread(sl1_f, [1,100], '100*uchar')));    
    attributes.site_name = deblank(char(fread(sl1_f, [1,100], '100*uchar')));    
    attributes.comment = deblank(char(fread(sl1_f, [1,100], '100*uchar')));    
    attributes.source = deblank(char(fread(sl1_f, [1,100], '100*uchar')));    

    % header end identifier
    end_identifier = fread(sl1_f, 1, 'uchar');
    end_identifier_check = isequal(...
        end_identifier,...
        3);
    if (end_identifier_check == false)
        disp('Failed header end identifier check.');
        attributes = struct;
        return
    end

    % header length check
    current_pos = ftell(sl1_f);
    header_length_check = isequal(...
        current_pos,...
        header_length);
    if (header_length_check == false)
        disp('Failed header length check.');
        attributes = struct;
        return
    end
end
    
function sl1_block_header = sl1_read_block_header(sl1_f, attributes)
    % Read sl1 block header.
    
    sl1_block_header = struct;
    
    if feof(sl1_f)    
        return
    end
        
    % block header identifier
    binary_word = char(fread(sl1_f, [1,3], '3*uchar'));

    if feof(sl1_f)    
        return
    end
    sl1_file_identifier_check = isequal(...
        binary_word,...
        'dbh');
    if (sl1_file_identifier_check == false)
        disp('Invalid block header start identifier.');
        return
    end    
    
    sl1_block_header.utc_unixtimestamp = fread(sl1_f, 1, 'float64');        
    sl1_block_header.antenna_azimuth_angle_rad = fread(sl1_f, 1, 'float32');
    sl1_block_header.antenna_elevation_angle_rad = fread(sl1_f, 1, 'float32');    
    sl1_block_header.transmit_pow_W = fread(sl1_f, 1, 'float32');    
    sl1_block_header.attenuation_H_dB = fread(sl1_f, 1, 'float32');    
    sl1_block_header.attenuation_V_dB = fread(sl1_f, 1, 'float32');    
    sl1_block_header.temperature_1_C = fread(sl1_f, 1, 'float32');    
    sl1_block_header.temperature_2_C = fread(sl1_f, 1, 'float32');    
    
    end_identifier = fread(sl1_f, 1, 'uchar');
    
    % nubmer of byte details
    sl1_block_header.n_bytes_block_header = (...
        3 + 8 + 7 * 4 + 1);

    sl1_block_header.n_bytes_block_data = (...
        4 * attributes.n_dopfreq * ...
        attributes.n_samples * length(attributes.valid_channel_tags));

    if attributes.data_is_complex
        sl1_block_header.n_bytes_block_data = (...
            2 * sl1_block_header.n_bytes_block_data);
    end
end

function sl1_block_data = sl1_read_block_data(...
        sl1_f, attributes, sl1_block_header)
    % read block data
    sl1_block_data = struct;
    n_points = attributes.n_dopfreq * attributes.n_samples;

    % HH
    if ismember("HH", attributes.valid_channel_tags)
        sl1_block_data.data_HH = fread(sl1_f, n_points, 'float32');            
        if attributes.data_is_complex
            sl1_block_data.data_HH = (...
                sl1_block_data.data_HH + ...
                (1.j * fread(sl1_f, n_points, 'float32')));          
        end
    end
    
    % HV
    if ismember("HV", attributes.valid_channel_tags)
        sl1_block_data.data_HV = fread(sl1_f, n_points, 'float32');            
        if attributes.data_is_complex
            sl1_block_data.data_HV = (...
                sl1_block_data.data_HV + ...
                (1.j * fread(sl1_f, n_points, 'float32')));          
        end
    end

    % VH
    if ismember("VH", attributes.valid_channel_tags)
        sl1_block_data.data_VH = fread(sl1_f, n_points, 'float32');            
        if attributes.data_is_complex
            sl1_block_data.data_VH = (...
                sl1_block_data.data_VH + ...
                (1.j * fread(sl1_f, n_points, 'float32')));          
        end
    end
    
    % VV
    if ismember("VV", attributes.valid_channel_tags)
        sl1_block_data.data_VV = fread(sl1_f, n_points, 'float32');            
        if attributes.data_is_complex
            sl1_block_data.data_VV = (...
                sl1_block_data.data_VV + ...
                (1.j * fread(sl1_f, n_points, 'float32')));          
        end
    end
end

function next_sl1_filename = get_next_sl1_filename(...
        sl1_filename)   
    n = strlength(sl1_filename);
    str_num = extractBetween(sl1_filename,n-12,n-4);
    num = str2num(str_num);
    new_num = num + 1;
    str_new_num = sprintf('%09d', new_num);

    next_sl1_filename = ...
        extractBefore(sl1_filename,n-12) + ...
        str_new_num + ...
        extractAfter(sl1_filename,n-4);
end

function extra_inf = calc_extra_inf(attributes, sl1_block_header)
    extra_inf = struct 
    
    % parameters
    speed_of_light_ms = 2.99792458e8

    % derived paramters
    extra_inf.n_range = attributes.n_samples / 2
    extra_inf.wavel_m = (...
        speed_of_light_ms / (attributes.central_frequency_GHz * 1.e9))

    % Calculate range paramters
    extra_inf.range_resolution_m = (...
        speed_of_light_ms / (attributes.bandwidth_MHz * 1.e6))
    if attributes.dual_transmission
        extra_inf.range_resolution_m = (...
            extra_inf.range_resolution_m / 2.)
    end
    extra_inf.range_m = ...
        ([0:extra_inf.n_range-1] + 0.5) * extra_inf.range_resolution_m
    extra_inf.max_range_m = extra_inf.range_m(end)

    % Calculate doppler velocity parameters
    % - doppler_velocity_max_ms is the maximum Doppler velocity for single pol radar
    % - doppler_velocity_tot_ms is the total range of Doppeler velocities for single pol radar
    % - (2 * vmax, as the spectrum goes from -vmax until +vmax)
    % - for dual pol the maximum Doppler velocity is divided by two.
    % (this is because the time between consecutive sweeps doubles).

    extra_inf.doppler_velocity_max_ms = ...
        speed_of_light_ms / (...
            4. * 1.e3 * attributes.central_frequency_GHz * ...
            attributes.sweep_time_mus)
    extra_inf.doppler_velocity_tot_ms = ...
        2.0 * extra_inf.doppler_velocity_max_ms
    extra_inf.doppler_velocity_step_ms = (...
           extra_inf.doppler_velocity_tot_ms / attributes.n_dopfreq)
    if attributes.dual_transmission
        extra_inf.doppler_velocity_max_ms = (...
            extra_inf.doppler_velocity_max_ms / 2.)
        extra_inf.doppler_velocity_tot_ms = (...
            extra_inf.doppler_velocity_tot_ms / 2.)
        extra_inf.doppler_velocity_step_ms = (...
            extra_inf.doppler_velocity_step_ms / 2.)
    end
    
    halfDop = floor(attributes.n_dopfreq / 2)
    extra_inf.doppler_velocity_ms = ...
        ([0:attributes.n_dopfreq-1] - halfDop) * ...
        extra_inf.doppler_velocity_step_ms

    % radar constant calculation
    % - not taken into account fractional sweep usage (due to sweep truncation).
    % - taking into account receiver attenuation
    % - taking account calibration offsets
    
    crad_part1_lin_HH = (1.e18 ...
        * db2pow(attributes.radar_constant_factor_dB_HH) ...
        * db2pow(sl1_block_header.attenuation_H_dB) ...
        * db2pow(attributes.calibration_offset_dB_HH) ...
        / sl1_block_header.transmit_pow_W)

    crad_part1_lin_HV = (1.e18 ...
        * db2pow(attributes.radar_constant_factor_dB_HV) ...
        * db2pow(sl1_block_header.attenuation_V_dB) ...
        * db2pow(attributes.calibration_offset_dB_HV) ...
        / sl1_block_header.transmit_pow_W)
    
    crad_part1_lin_VH = (1.e18 ...
        * db2pow(attributes.radar_constant_factor_dB_VH) ...
        * db2pow(sl1_block_header.attenuation_H_dB) ...
        * db2pow(attributes.calibration_offset_dB_VH) ...
        / sl1_block_header.transmit_pow_W)
    
    crad_part1_lin_VV = (1.e18 ...
        * db2pow(attributes.radar_constant_factor_dB_VV) ...
        * db2pow(sl1_block_header.attenuation_V_dB) ...
        * db2pow(attributes.calibration_offset_dB_VV) ...
        / sl1_block_header.transmit_pow_W)    

    extra_inf.radar_constant_lin_HH = (...
        crad_part1_lin_HH * (extra_inf.range_m .^2))
    extra_inf.radar_constant_lin_HV = (...
        crad_part1_lin_HV * (extra_inf.range_m .^2))
    extra_inf.radar_constant_lin_VH = (...
        crad_part1_lin_VH * (extra_inf.range_m .^2))
    extra_inf.radar_constant_lin_VV = (...
        crad_part1_lin_VV * (extra_inf.range_m .^2))
end
