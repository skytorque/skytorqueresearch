"""
radar_filenames.py
- A python script from the SkyEcho Radar Python library (skyrapy).
- Generate radar folder and radar filenames.

Update history
- First version (AON, 2020).

Copyright 2021 SkyEcho

Usage examples:
1. Run an example.
$ python radar_filenames.py run_example
"""

import sys
import os
import pprint

def get_dct_folders(
        root_folder=None):
    """Return dictionary with all radar folders."""

    if root_folder is None:
        return None

    root_folder = os.path.normpath(os.path.realpath(root_folder))
    dct_folders = {}
    dct_folders["level1_data"] = root_folder + "/level1_data/"
    dct_folders["level1_noise"] = root_folder + "/level1_noise/"
    dct_folders["level2_archive"] = root_folder + "/level2_archive/"
    dct_folders["level2pp_archive"] = root_folder + "/level2pp_archive/"
    dct_folders["level2_single_scan"] = root_folder + "/level2_single_scan/"
    dct_folders["level2pp_single_scan"] = root_folder + "/level2pp_single_scan/"
    dct_folders["level2_quicklooks"] = root_folder + "/level2_quicklooks/"
    dct_folders["level3pp_archive"] = root_folder + "/level3pp_archive/"
    dct_folders["level3pp_single_scan"] = root_folder + "/level3pp_single_scan/"
    dct_folders["level3_quicklooks"] = root_folder + "/level3_quicklooks/"
    dct_folders["log_files"] = root_folder + "/log_files/"

    return dct_folders

def get_filename(
        root_folder=None,
        folder_tag="level2_quicklooks",
        file_tag="level2_statistics_quicklook",
        radar_tag="RIJNMOND",
        utc_fmt_file="2020-05-02T00-00-00Z",
        end_tag=None,
        filename_ext="png",
        assert_output_folder_exists=True):
    """Get filename and folder for a radar file."""
    if root_folder is None:
        return None

    root_folder = os.path.normpath(os.path.realpath(root_folder))
    folder = root_folder + "/" + folder_tag + "/"

    # get plot filename
    filename = folder+(
        "/{}_{}_{}".format(
            file_tag,
            radar_tag,
            utc_fmt_file))

    if end_tag is not None:
        filename += "_" + end_tag

    # add extension
    filename += "." + filename_ext

    # assert that the output folder exists
    if assert_output_folder_exists:
        os.makedirs(
            folder,
            exist_ok=True)

    return folder, filename


def run_example():
    """Basic example."""
    pprint.pprint(get_filename(root_folder="/data/test", assert_output_folder_exists=False))
    pprint.pprint(get_dct_folders(root_folder="./"))


if __name__ == '__main__':
    user_action = None
    user_filename = None

    if len(sys.argv) > 1:
        user_action = sys.argv[1]
    if len(sys.argv) > 2:
        user_filename = sys.argv[2]

    if user_action == "run_example":
        run_example()

    if user_action is None:
        # print documentation
        print(__doc__)
