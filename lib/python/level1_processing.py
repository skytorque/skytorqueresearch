"""
level1_processing.py
- A python script from the SkyEcho Radar Python library (skyrapy).
- Spectral processing and analysis of multi-channel level1 data.
- This script is used to reprocess mcl1 data, and to further develop the spectral processing.

Copyright 2021 SkyEcho.

Update history
- First version (AON, 2020).

- Major overhaul (AON, 2020-03)
-- Usage of sl1 files.
-- FFT calculations updated.
-- noise estimation from active spectra added.
-- Absolute sZDR filter added.
-- correlation calculation added.
-- correlation filter added.
-- speckle filter added.
-- overview texts added.
-- differential phase correction calculation reviewed, and implemented properly.
-- Doppler dealiasing reviewed.
-- versions available for 2019, 2020, 2021.
-- Adaptive thresholding for correlation coefficient added.
-- SNR, and noise added to radar moments.
 
whish-list:
- Doppler dealiasing improvements.
- Evaluation methods from the thesis from Cheng.
- Trials with different range_step in the adaptive thresholding for correlation coefficient.
- Trials with different kernel size for the correlation coefficient calculation.
- Determine system phase offsets for the different radar systems
- Review SNR calculation, which might be improved.

- selection of azimuth, in the level1_io.
- save to level2 file, consequently ppi plot.

- more examples.
"""

import datetime
import sys
import os
import re

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import scipy
import scipy.signal
import skimage
import skimage.morphology
from mpl_toolkits.axes_grid1 import make_axes_locatable

import level1_io
import radar_filenames

# skyrapy plot dir
skyrapy_plot_dir = os.path.expanduser("~/skyrapy/plots/")
os.makedirs(skyrapy_plot_dir, exist_ok=True)

radardata_channel_tags = ["HH", "HV", "VH", "VV"]

dct_sp_settings_v2021_rijnmond_typical = {
    # sweep truncation
    "truncate_first_min_n_sweep_samples": 20,
    "truncate_first_percentage_sweep_samples": 15,

    # fft settings
    "fft_rangedomain_windowing": "Chebyshev",
        # -> Chebyshev for super low sidelobes, superior contrast
    "fft_dopplerdomain_windowing": "Chebyshev",
        # -> Chebyshev for super low sidelobes, superior contrast

    # noise filtering
    "noise_filtering": "from_spectra",
    "noise_filtering_clipping_threshold_db": 3.0,
    "noise_filtering_clipping_threshold_std": 1.0,

    # spectral filters
    "zeroDopplerwidthfilter_width_ms": 0.1,
    "sLDR_HV_filter": False,
    "sLDR_VH_filter": False,
    "abs_sZDR_filter": True,
    "abs_sZDR_filter_threshold_db": 12.0,
    "copolar_correlation_filter": True,
    "copolar_correlation_filter_automatic_threshold": True,
    "speckle_filter": True,

    # data discarding
    "discard_cells_with_little_doppler_bins": True,
    "discard_cells_with_little_doppler_bins_percentage": 2.0,
    "discard_saturated_data": True,
    "discard_saturated_data_treshold_percentage": 25.0,
    "discard_cells_with_low_copolar_correlation": False,
    "discard_cells_with_high_LDR": True,
    "discard_cells_with_high_LDR_threshold": -7.0,

    # doppler dealiasing
    "doppler_dealiasing_use_differential_phase": False,
}

def spectral_processing_v2021(
        mcl1_data,
        mcl1_noise=None,
        dct_sp_settings_in={},
        verbose=True):
    """
    Spectral processing, version 2021.

    Input:
    - mcl1_data
    - mcl1_noise (optional, required in case noise filtering is "from_measurement")
    - dct_sp_settings_in (optional)

    Output:
    - dct_sp_result."""

    dct_sp_result = {}

    # take default settings
    # override with user settings
    dct_sp_settings = {}
    dct_sp_settings.update(
        dct_sp_settings_v2021_rijnmond_typical)
    dct_sp_settings.update(dct_sp_settings_in)

    # get a textual overview of the settings
    settings_overview_str = get_settings_overview_str(dct_sp_settings)
    dct_sp_result["settings_overview_str"] = settings_overview_str

    # get a textual overview of the data
    mcl1_data_overview_str = get_ml1_data_overview_str(mcl1_data)
    dct_sp_result["mcl1_data_overview_str"] = mcl1_data_overview_str

    if verbose:
        print(settings_overview_str)
        print(mcl1_data_overview_str)

    #
    #
    # Here the spectral processing starts
    do_prep_data(dct_sp_result, mcl1_data, mcl1_noise)
        # prepare data (copy, reshaping)

    do_set_truncate_first_n_sweep_samples(
        dct_sp_result, dct_sp_settings)
    do_calc_saturation(
        dct_sp_result, dct_sp_settings)
    do_calc_rx_power(
        dct_sp_result, dct_sp_settings)

    do_fft_etc(
        dct_sp_result, dct_sp_settings)
        # - removal of the mean
        # - windowing
        # - fft
    do_fft_shift(
        dct_sp_result, dct_sp_settings)
        # - fft shift in the Doppler axis

    do_apply_radar_constant(
        dct_sp_result, dct_sp_settings)
        # - apply radar constant to the spectra
        # - and apply callibration constants

    do_zerodoppler_clutterfilter(
        dct_sp_result, dct_sp_settings)
    do_calc_rangedoppler_reflectivity_linear(
        dct_sp_result, dct_sp_settings)

    do_set_noise_and_noise_clipping_levels(
        dct_sp_result, dct_sp_settings)

    do_calc_rangedoppler_phase(
        dct_sp_result, dct_sp_settings)
    do_calc_rangedoppler_correlation_coefficients(
        dct_sp_result, dct_sp_settings)

    do_sLDR_filter(
        dct_sp_result, dct_sp_settings)
    do_abs_sZDR_filter(
        dct_sp_result, dct_sp_settings)
    do_copolar_correlation_filter(
        dct_sp_result, dct_sp_settings)
    do_speckle_filter(
        dct_sp_result, dct_sp_settings)

    do_doppler_dealiasing(
        dct_sp_result, dct_sp_settings)
    do_moments_calculation(
        dct_sp_result, dct_sp_settings)
    do_data_discarding(
        dct_sp_result, dct_sp_settings)

    # get a textual overview of the analysis
    sp_analysis_overview_str = get_sp_analysis_overview_str(dct_sp_result)
    dct_sp_result["sp_analysis_overview_str"] = sp_analysis_overview_str

    if verbose:
        print(sp_analysis_overview_str)

    return dct_sp_result, dct_sp_settings


dct_sp_settings_v2020_rijnmond_typical = {
    # sweep truncation
    "truncate_first_min_n_sweep_samples": 20,
    "truncate_first_percentage_sweep_samples": 15,

    # fft settings
    "fft_rangedomain_windowing": "Hamming",
    "fft_dopplerdomain_windowing": "Hamming",

    # noise filtering
    "noise_filtering": "from_spectra",
    "noise_filtering_clipping_threshold_db": 3.0,
    "noise_filtering_clipping_threshold_std": 1.0,

    # spectral filters
    "zeroDopplerwidthfilter_width_ms": 0.1,
    "sLDR_HV_filter": True,
    "sLDR_HV_filter_threshold_db": -7.,
    "sLDR_VH_filter": True,
    "sLDR_VH_filter_threshold_db": -7.,
    "abs_sZDR_filter": True,
    "abs_sZDR_filter_threshold_db": 12.0,
    "copolar_correlation_filter": False,
    "speckle_filter": True,

    # data discarding
    "discard_cells_with_little_doppler_bins": True,
    "discard_cells_with_little_doppler_bins_percentage": 2.0,
    "discard_saturated_data": True,
    "discard_saturated_data_treshold_percentage": 25.0,
    "discard_cells_with_low_copolar_correlation": False,
    "discard_bad_spectral_quality_index": True,
    "discard_bad_spectral_quality_index_treshold": 0.4,
}


def spectral_processing_v2020(
        mcl1_data,
        mcl1_noise=None,
        dct_sp_settings_in={},
        verbose=True):
    """Spectral processing. Depracated version 2020.
    - See details in v2021."""

    dct_sp_result = {}

    dct_sp_settings = {}
    dct_sp_settings.update(
        dct_sp_settings_v2020_rijnmond_typical)
    dct_sp_settings.update(dct_sp_settings_in)

    settings_overview_str = get_settings_overview_str(dct_sp_settings)
    dct_sp_result["settings_overview_str"] = settings_overview_str

    mcl1_data_overview_str = get_ml1_data_overview_str(mcl1_data)
    dct_sp_result["mcl1_data_overview_str"] = mcl1_data_overview_str

    if verbose:
        print(settings_overview_str)
        print(mcl1_data_overview_str)

    do_prep_data(dct_sp_result, mcl1_data, mcl1_noise)
    do_truncate_sweep_samples_v2019(
        dct_sp_result, dct_sp_settings)
    do_calc_saturation(
        dct_sp_result, dct_sp_settings)
    do_calc_rx_power(
        dct_sp_result, dct_sp_settings)
    do_fft_etc_v2019(
        dct_sp_result, dct_sp_settings)
    do_fft_shift(
        dct_sp_result, dct_sp_settings)
    do_apply_radar_constant(
        dct_sp_result, dct_sp_settings)
    do_zerodoppler_clutterfilter(
        dct_sp_result, dct_sp_settings)
    do_calc_rangedoppler_reflectivity_linear(
        dct_sp_result, dct_sp_settings)
    do_set_noise_and_noise_clipping_levels(
        dct_sp_result, dct_sp_settings, method="version2020")
    do_calc_rangedoppler_phase(
        dct_sp_result, dct_sp_settings)
    do_calc_rangedoppler_correlation_coefficients(
        dct_sp_result, dct_sp_settings,
        apply_phase_correction=False)
    do_sLDR_filter(
        dct_sp_result, dct_sp_settings)
    do_abs_sZDR_filter(
        dct_sp_result, dct_sp_settings)
    do_copolar_correlation_filter(
        dct_sp_result, dct_sp_settings)
    do_speckle_filter(
        dct_sp_result, dct_sp_settings, kernel_size=3)
    do_calc_spectral_quality_index_v2020(
        dct_sp_result, dct_sp_settings)
    do_moments_calculation(
        dct_sp_result, dct_sp_settings)
    do_data_discarding(
        dct_sp_result, dct_sp_settings)

    sp_analysis_overview_str = get_sp_analysis_overview_str(dct_sp_result)
    dct_sp_result["sp_analysis_overview_str"] = sp_analysis_overview_str

    if verbose:
        print(sp_analysis_overview_str)

    return dct_sp_result, dct_sp_settings


dct_sp_settings_v2019_rijnmond_typical = {
    # sweep truncation
    "truncate_first_min_n_sweep_samples": 100,
    "truncate_first_percentage_sweep_samples": 10,

    # fft settings
    "fft_rangedomain_windowing": "Hamming",
    "fft_dopplerdomain_windowing": "Hamming",

    # noise filtering
    "noise_filtering": "from_measurement",
    "noise_filtering_clipping_threshold_db": 3.0,

    # spectral filters
    "zeroDopplerwidthfilter_width_ms": 0.2,
    "sLDR_HV_filter": True,
    "sLDR_HV_filter_threshold_db": -7.,
    "sLDR_VH_filter": True,
    "sLDR_VH_filter_threshold_db": -7.,

    # data discarding
    "discard_cells_with_little_doppler_bins": True,
    "discard_cells_with_little_doppler_bins_percentage": 2.0,
}


def spectral_processing_v2019(
        mcl1_data,
        mcl1_noise=None,
        dct_sp_settings_in={},
        verbose=True):
    """Spectral processing. Depracated version 2019.
    - See details in v2021."""

    dct_sp_result = {}

    dct_sp_settings = {}
    dct_sp_settings.update(
        dct_sp_settings_v2019_rijnmond_typical)
    dct_sp_settings.update(dct_sp_settings_in)

    settings_overview_str = get_settings_overview_str(dct_sp_settings)
    dct_sp_result["settings_overview_str"] = settings_overview_str

    mcl1_data_overview_str = get_ml1_data_overview_str(mcl1_data)
    dct_sp_result["mcl1_data_overview_str"] = mcl1_data_overview_str

    if verbose:
        print(settings_overview_str)
        print(mcl1_data_overview_str)

    do_prep_data(dct_sp_result, mcl1_data, mcl1_noise)
    do_truncate_sweep_samples_v2019(
        dct_sp_result, dct_sp_settings)
    do_fft_etc_v2019(
        dct_sp_result, dct_sp_settings)
    do_fft_shift(
        dct_sp_result, dct_sp_settings)
    do_apply_radar_constant(
        dct_sp_result, dct_sp_settings)
    do_zerodoppler_clutterfilter(
        dct_sp_result, dct_sp_settings)
    do_calc_rangedoppler_reflectivity_linear(
        dct_sp_result, dct_sp_settings)
    do_set_noise_and_noise_clipping_levels(
        dct_sp_result, dct_sp_settings, method="version2019")
    do_calc_rangedoppler_phase(
        dct_sp_result, dct_sp_settings)
    do_sLDR_filter(
        dct_sp_result, dct_sp_settings)
    do_moments_calculation(
        dct_sp_result, dct_sp_settings)
    do_data_discarding(
        dct_sp_result, dct_sp_settings)

    # get a textual overview of the analysis
    sp_analysis_overview_str = get_sp_analysis_overview_str(dct_sp_result)
    dct_sp_result["sp_analysis_overview_str"] = sp_analysis_overview_str

    if verbose:
        print(sp_analysis_overview_str)

    return dct_sp_result, dct_sp_settings


settings_overview_str_template = \
"""
Spectral processing settings.

- Sweep settings:
-- truncate first min_n sweep samples:      <truncate_first_min_n_sweep_samples>
-- truncate first percentage sweep_samples: <truncate_first_percentage_sweep_samples>

- FFT settings:
-- fft_rangedomain_windowing:                   <fft_rangedomain_windowing>
-- fft_rangedomain_windowing_parameter1:        <fft_rangedomain_windowing_parameter1>
-- fft_dopplerdomain_windowing:                 <fft_dopplerdomain_windowing>
-- fft_dopplerdomain_windowing_parameter1:      <fft_dopplerdomain_windowing_parameter1>

- Noise filtering
-- Method:                  <noise_filtering>
-- Clipping threshold [dB]: <noise_filtering_clipping_threshold_db>
-- Clipping threshold std:  <noise_filtering_clipping_threshold_std>

- Filters
-- zero Doppler width filter, width [ms]:           <zeroDopplerwidthfilter_width_ms>
-- sLDR HV filter:                                  <sLDR_HV_filter>
-- sLDR HV filter, treshold [dB]:                   <sLDR_HV_filter_threshold_db>
-- sLDR VH filter:                                  <sLDR_VH_filter>
-- sLDR VH filter, treshold [dB]:                   <sLDR_VH_filter_threshold_db>
-- abs. sZDR treshold:                              <abs_sZDR_filter>
-- abs. sZDR treshold [dB]:                         <abs_sZDR_filter_threshold_db>
-- copolar correlation filter:                      <copolar_correlation_filter>
-- copolar correlation, threshold:                  <copolar_correlation_filter_threshold>
-- copolar_correlation_filter_automatic_threshold:  <copolar_correlation_filter_automatic_threshold>
-- speckle filter:                                  <speckle_filter>

- Data discarding
-- cells with little Doppler bins:                  <discard_cells_with_little_doppler_bins>
-- cells with little Doppler bins, percentage:      <discard_cells_with_little_doppler_bins_percentage>
-- saturated data:                                  <discard_saturated_data>
-- saturated data, threshold [%]:                   <discard_saturated_data_treshold_percentage>
-- cells with low copolar correlation:              <discard_cells_with_low_copolar_correlation>
-- cells with low copolar correlation, threshold:   <discard_cells_with_low_copolar_correlation_threshold>
-- discard_bad_spectral_quality_index:              <discard_bad_spectral_quality_index>
-- discard_bad_spectral_quality_index_treshold:     <discard_bad_spectral_quality_index_treshold>
"""


def get_settings_overview_str(
        dct_sp_settings,
        report_key_usage=False):
    """Get settings overview string."""
    global ettings_overview_str_template

    settings_overview_str = ""

    keys_in_template_not_used = []
    keys_in_settings_used = []
    for line in settings_overview_str_template.split("\n"):

        m = re.search(r"\<(.*?)\>", line)
        if m is not None:
            # line with a key
            this_key = m.group(1)

            if this_key in dct_sp_settings:
                # -> take over this line
                this_value = dct_sp_settings[this_key]
                new_line = line.replace(
                    "<{}>".format(this_key),
                    str(this_value))
                settings_overview_str += new_line + "\n"

                keys_in_settings_used.append(this_key)
            else:
                keys_in_template_not_used.append(this_key)
                # -> skip this line
        else:
            # line without a tag
            # -> take over the line
            settings_overview_str += line + "\n"

    keys_in_settings_not_used = list(set(dct_sp_settings.keys()) - set(keys_in_settings_used))

    if report_key_usage:
        print("Keys not used in template: ", keys_in_template_not_used)
        print("Keys not used from settings: ", keys_in_settings_not_used)

    return settings_overview_str


mcl1_data_overview_str_template = \
"""
mcl1 data overview

instrument_name: <instrument_name>
latitude_deg: <latitude_deg>
longitude_deg: <longitude_deg>
altitude_m: <altitude_m>

time:                   <utc_timestr>
azimuth angle [deg]:    <antenna_azimuth_angle_deg>
elevation angle [deg]:  <antenna_elevation_angle_deg>

bandwidth_MHz:          <bandwidth_MHz>
central_frequency_GHz:  <central_frequency_GHz>
sweep_time_mus:         <sweep_time_mus>
dual_transmission:      <dual_transmission>

transmit power [W]: <transmit_pow_W>
attenuation H [dB]: <attenuation_H_dB>
attenuation V [dB]: <attenuation_V_dB>

n_samples:  <n_samples>
n_range:    <n_range>
n_dopfreq:  <n_dopfreq>
"""


def get_ml1_data_overview_str(
        mcl1_data,
        report_key_usage=False):
    """Get mcl1 data overview string."""
    global mcl1_data_overview_str_template

    # make temporary dct with some additional keys
    temp_dct = {
        "utc_timestr": datetime.datetime.utcfromtimestamp(
            mcl1_data["utc_unixtimestamp"]).strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
        "antenna_azimuth_angle_deg": np.rad2deg(mcl1_data["antenna_azimuth_angle_rad"]),
        "antenna_elevation_angle_deg": np.rad2deg(mcl1_data["antenna_elevation_angle_rad"]),}
    temp_dct.update(mcl1_data)

    mcl1_data_overview_str = ""
    keys_in_template_not_used = []
    keys_in_dct_used = []

    for line in mcl1_data_overview_str_template.split("\n"):

        m = re.search(r"\<(.*?)\>", line)
        if m is not None:
            # line with a key
            this_key = m.group(1)

            if this_key in temp_dct:
                # -> take over this line
                this_value = temp_dct[this_key]
                new_line = line.replace(
                    "<{}>".format(this_key),
                    str(this_value))
                mcl1_data_overview_str += new_line + "\n"

                keys_in_dct_used.append(this_key)
            else:
                keys_in_template_not_used.append(this_key)
                # -> skip this line
        else:
            # line without a tag
            # -> take over the line
            mcl1_data_overview_str += line + "\n"

    keys_in_dct_not_used = list(set(temp_dct.keys()) - set(keys_in_dct_used))

    if report_key_usage:
        print("Keys not used in template: ", keys_in_template_not_used)
        print("Keys not used from dct: ", keys_in_dct_not_used)

    return mcl1_data_overview_str


sp_analysis_overview_str_template = \
"""
analysis overview

- saturation
-- saturation_HH [%]:   <saturation_percentage_HH>
-- saturation_HV [%]:   <saturation_percentage_HV>
-- saturation_VH [%]:   <saturation_percentage_VH>
-- saturation_VV [%]:   <saturation_percentage_VV>

- receiver power
-- receiver_pow_HH [dBm]:   <receiver_pow_dBm_HH>
-- receiver_pow_HV [dBm]:   <receiver_pow_dBm_HV>
-- receiver_pow_VH [dBm]:   <receiver_pow_dBm_VH>
-- receiver_pow_VV [dBm]:   <receiver_pow_dBm_VV>
"""


def get_sp_analysis_overview_str(
        dct_sp_result,
        report_key_usage=False):
    """Get spectral processing analysis overview string."""
    global sp_analysis_overview_str_template

    temp_dct = {}
    temp_dct.update(dct_sp_result["data"])
    temp_dct.update(dct_sp_result)

    sp_analysis_overview_str = ""
    keys_in_template_not_used = []
    keys_in_dct_used = []

    for line in sp_analysis_overview_str_template.split("\n"):

        m = re.search(r"\<(.*?)\>", line)
        if m is not None:
            # line with a key
            this_key = m.group(1)

            if this_key in temp_dct:
                # -> take over this line
                this_value = temp_dct[this_key]
                new_line = line.replace(
                    "<{}>".format(this_key),
                    str(this_value))
                sp_analysis_overview_str += new_line + "\n"

                keys_in_dct_used.append(this_key)
            else:
                keys_in_template_not_used.append(this_key)
                # -> skip this line
        else:
            # line without a tag
            # -> take over the line
            sp_analysis_overview_str += line + "\n"

    keys_in_dct_not_used = list(set(temp_dct.keys()) - set(keys_in_dct_used))

    if report_key_usage:
        print("Keys not used in template: ", keys_in_template_not_used)
        print("Keys not used from dct: ", keys_in_dct_not_used)

    return sp_analysis_overview_str


def do_prep_data(
        dct_sp_result,
        mcl1_data,
        mcl1_noise=None):
    """
    Prepare data.
    - copy the data
    - reshape the data
    """

    # take over some essential parameters
    for myvar in [
            "instrument_name",
            "utc_unixtimestamp",

            "bandwidth_MHz",
            "central_frequency_GHz",
            "sweep_time_mus",
            "dual_transmission",

            "n_dopfreq",
            "n_samples",
            "n_range",
            "range_m",
            "doppler_velocity_ms",

            "wavel_m",
            "doppler_velocity_max_ms",
            "doppler_velocity_tot_ms",
            ]:
        dct_sp_result[myvar] = mcl1_data[myvar]

    data_shape = mcl1_data["n_dopfreq"], mcl1_data["n_samples"]
    data_shape_2 = mcl1_data["n_dopfreq"], mcl1_data["n_range"]
    dct_sp_result["data_shape"] = data_shape
    dct_sp_result["data_shape_2"] = data_shape_2
    dct_sp_result["moments"] = {}

    n_tot = np.product(data_shape)

    # initialize rangedoppler_filter_combined
    dct_sp_result["rangedoppler_filter_combined"] = np.zeros(
        data_shape_2, dtype=bool)

    my_n = np.product(data_shape)

    if mcl1_data is not None:
        dct_sp_result["data"] = {}

        for channel_tag in radardata_channel_tags:
            myvar = "rawdata_linear_" + channel_tag

            if myvar not in mcl1_data.keys():
                continue

            dct_sp_result["data"][myvar] = mcl1_data[myvar].reshape(data_shape, order="C")

            # take over radar constant
            my_constant_var = "radar_constant_lin_" + channel_tag
            dct_sp_result["data"][my_constant_var] = mcl1_data[my_constant_var]

            # take over max abs value
            my_max_abs_var = "max_abs_value_" + channel_tag
            dct_sp_result["data"][my_max_abs_var] = mcl1_data[my_max_abs_var]

    if mcl1_noise is not None:
        dct_sp_result["noise"] = {}

        for channel_tag in radardata_channel_tags:
            myvar = "rawdata_linear_" + channel_tag

            if myvar not in mcl1_noise.keys():
                continue

            dct_sp_result["noise"][myvar] = mcl1_noise[myvar][:n_tot].reshape(data_shape, order="C")

            # take over radar constant from mcl1_data
            my_constant_var = "radar_constant_lin_" + channel_tag
            dct_sp_result["noise"][my_constant_var] = mcl1_data[my_constant_var]

            # take over max abs value
            my_max_abs_var = "max_abs_value_" + channel_tag
            dct_sp_result["noise"][my_max_abs_var] = mcl1_noise[my_max_abs_var]


def do_set_truncate_first_n_sweep_samples(
        dct_sp_result, dct_sp_settings):
    """Determine truncate_first_n_sweep_samples. This is used in the windowing function."""

    # calculate truncate_first_n_sweep_samples
    truncate_first_n_sweep_samples = int(
        (dct_sp_settings["truncate_first_percentage_sweep_samples"] / 100.) *
        dct_sp_result["n_samples"])
    if (truncate_first_n_sweep_samples <
            dct_sp_settings["truncate_first_min_n_sweep_samples"]):
        truncate_first_n_sweep_samples = \
            dct_sp_settings["truncate_first_min_n_sweep_samples"]
    dct_sp_settings["truncate_first_n_sweep_samples"] = truncate_first_n_sweep_samples


def do_truncate_sweep_samples_v2019(
        dct_sp_result, dct_sp_settings):
    """Truncate first sweep samples. Depracated."""

    # calculate truncate_first_n_sweep_samples
    truncate_first_n_sweep_samples = int(
        (dct_sp_settings["truncate_first_percentage_sweep_samples"] / 100.) *
        dct_sp_result["n_samples"])
    if (truncate_first_n_sweep_samples <
            dct_sp_settings["truncate_first_min_n_sweep_samples"]):
        truncate_first_n_sweep_samples = \
            dct_sp_settings["truncate_first_min_n_sweep_samples"]
    dct_sp_settings["truncate_first_n_sweep_samples"] = truncate_first_n_sweep_samples

    for my_key in ["data", "noise"]:
        if not my_key in dct_sp_result:
            continue

        my_data_dct = dct_sp_result[my_key]

        for channel_tag in radardata_channel_tags:
            myvar = "rawdata_linear_" + channel_tag

            if myvar not in my_data_dct.keys():
                continue

            # sweep truncation
            my_data_dct[myvar][:, 0:truncate_first_n_sweep_samples] = 0.

            # correction
            correction_factor_pow = (
                dct_sp_result["n_samples"] /
                (1. * dct_sp_result["n_samples"] - truncate_first_n_sweep_samples))
            correction_factor_mag = np.sqrt(correction_factor_pow)

            # apply the correction
            my_data_dct[myvar] *= correction_factor_mag


def do_calc_saturation(
        dct_sp_result, dct_sp_settings):
    """Calculate saturation."""
    n_samples = dct_sp_result["n_samples"]
    n_dopfreq = dct_sp_result["n_dopfreq"]
    truncate_first_n_sweep_samples = dct_sp_settings["truncate_first_n_sweep_samples"]

    for my_key in ["data", "noise"]:
        if not my_key in dct_sp_result:
            continue

        my_data_dct = dct_sp_result[my_key]

        for channel_tag in radardata_channel_tags:
            myvar = "rawdata_linear_" + channel_tag

            if myvar not in my_data_dct.keys():
                continue

            my_data = my_data_dct[myvar][:, truncate_first_n_sweep_samples:]
            n_analyzed_samples = n_samples - truncate_first_n_sweep_samples

            max_abs_var_name = "max_abs_value_" + channel_tag
            new_var_name = "saturation_percentage_" + channel_tag

            saturation_threshold = (0.99 * my_data_dct[max_abs_var_name])

            my_data_dct[new_var_name] = (
                100. * np.sum(np.abs(my_data) > saturation_threshold) /
                (1. * n_dopfreq * n_analyzed_samples))


def do_calc_rx_power(
        dct_sp_result, dct_sp_settings):
    """Calculate receiver power."""

    truncate_first_n_sweep_samples = dct_sp_settings["truncate_first_n_sweep_samples"]
    n_samples = dct_sp_result["n_samples"]

    for my_key in ["data", "noise"]:
        if not my_key in dct_sp_result:
            continue

        my_data_dct = dct_sp_result[my_key]

        for channel_tag in radardata_channel_tags:
            myvar = "rawdata_linear_" + channel_tag

            if myvar not in my_data_dct.keys():
                continue

            # compensate for sweep truncation
            my_data = my_data_dct[myvar][:, truncate_first_n_sweep_samples:]
            fct_compensation = (
                n_samples /
                (1. * n_samples - truncate_first_n_sweep_samples))

            new_var_name = "receiver_pow_dBm_" + channel_tag
            receiver_pow_W = fct_compensation * np.abs(np.var(my_data))

            receiver_pow_dBm = dB(receiver_pow_W * 1.e3)
            my_data_dct[new_var_name] = receiver_pow_dBm


def do_fft_etc(
        dct_sp_result, dct_sp_settings):
    """Do FFT etc.
    - removal of the mean
    - windowing
    - fft."""

    n_samples = dct_sp_result["n_samples"]
    n_range = dct_sp_result["n_range"]
    n_dopfreq = dct_sp_result["n_dopfreq"]
    truncate_first_n_sweep_samples = dct_sp_settings["truncate_first_n_sweep_samples"]

    for my_key in ["data", "noise"]:
        if not my_key in dct_sp_result:
            continue

        my_data_dct = dct_sp_result[my_key]

        for channel_tag in radardata_channel_tags:
            myvar = "rawdata_linear_" + channel_tag

            if myvar not in my_data_dct.keys():
                continue

            this_data = my_data_dct[myvar]
            
            #
            #
            # range domain
            # - first remove the mean
            this_mean = np.mean(this_data, axis=1)
            this_data = this_data - this_mean[:, None]

            # - apply windowing
            # -- here sweep truncation is applied as well

            window_parameter1 = None
            if "fft_rangedomain_windowing_parameter1" in dct_sp_settings:
                window_parameter1 = dct_sp_settings["fft_rangedomain_windowing_parameter1"]

            this_scaled_window = get_scaled_window(
                n=dct_sp_result["n_samples"],
                i_start=truncate_first_n_sweep_samples,
                window_name=dct_sp_settings["fft_rangedomain_windowing"],
                window_parameter1=window_parameter1)
            if this_scaled_window is not None:
                this_data = (
                    this_data * this_scaled_window[None, :])

            # again remove the mean
            this_mean = np.mean(this_data, axis=1)
            this_data = this_data - this_mean[:, None]

            # - apply fft, and scale
            this_data = (
                np.fft.fft(this_data, axis=1) / dct_sp_result["n_samples"])

            # AON: updated calculation.
            # -- symmetrical (conjugate) upper part is added to the lower part
            # - in case of real data: power is conserved.
            # - in case of imaginary data: power and phase from both coefficients is conserved
            
            if n_samples % 2 == 1:
                # uneven n_samples
                max_mir_i = int((n_samples - 1) / 2)
            if n_samples % 2 == 0:
                # even n_samples        
                max_mir_i = int((n_samples / 2) - 1)              

            slice_A = slice(1, max_mir_i + 1)
            slice_B = slice(-1, -max_mir_i - 1, -1)
                
            this_data[:, slice_A] = (
                this_data[:, slice_A] + np.conjugate(this_data[:, slice_B])) / np.sqrt(2)
            this_data = this_data[:, :n_range]

            # i.e. np.var(x)
            # is equal to
            # np.sum(np.abs(x_fft[1:]/n)**2.)
            
            slice_0 = slice(0, n_range)
            slice_1 = slice(1, n_range)
            slice_2 = slice(-1, -n_range, -1)

            this_data = this_data[:, :n_range]
            this_data[:, slice_1] *= np.sqrt(2)

            #
            #
            # doppler domain
            # - first remove the mean
            this_mean = np.mean(this_data, axis=0)
            this_data = this_data - this_mean[None, :]

            # - apply windowing
            window_parameter1 = None
            if "fft_dopplerdomain_windowing_parameter1" in dct_sp_settings:
                window_parameter1 = dct_sp_settings["fft_dopplerdomain_windowing_parameter1"]
            this_scaled_window = get_scaled_window(
                n=n_dopfreq,
                window_name=dct_sp_settings["fft_dopplerdomain_windowing"],
                window_parameter1=window_parameter1)
            if this_scaled_window is not None:
                this_data = (
                    this_data * this_scaled_window[:, None])

            # again remove the mean
            this_mean = np.mean(this_data, axis=0)
            this_data = this_data - this_mean[None, :]            

            # - apply fft, and scale
            this_data = (
                np.fft.fft(this_data, axis=0) / n_dopfreq)

            # rename the variable
            new_name = "rangedoppler_" + channel_tag
            my_data_dct[new_name] = this_data

            del my_data_dct[myvar]


def do_fft_etc_v2019(
        dct_sp_result, dct_sp_settings):
    """Do FFT etc (version 2019)
    - removal of the mean
    - windowing
    - fft."""

    n_range = dct_sp_result["n_range"]
    n_dopfreq = dct_sp_result["n_dopfreq"]

    for my_key in ["data", "noise"]:
        if not my_key in dct_sp_result:
            continue

        my_data_dct = dct_sp_result[my_key]

        for channel_tag in radardata_channel_tags:
            myvar = "rawdata_linear_" + channel_tag

            if myvar not in my_data_dct.keys():
                continue

            #
            #
            # range domain
            # - first remove the mean
            this_data = my_data_dct[myvar]
            this_mean = np.mean(this_data, axis=1)
            this_data = this_data - this_mean[:, None]

            # - apply windowing
            # -- (AON: problematic scaling: scaling based on the variance, which is wrong.)
            this_scaled_window = get_scaled_window(
                n=dct_sp_result["n_samples"],
                window_name=dct_sp_settings["fft_rangedomain_windowing"],
                scaling_method="methodB")
            if this_scaled_window is not None:
                this_data = (
                    this_data * this_scaled_window[None, :])
                    
            # - apply fft, and scale
            this_data = (
                np.fft.fft(this_data, axis=1) / dct_sp_result["n_samples"])

            this_data = this_data[:, :n_range]

            # AON: issue detected as the upper fft part is thrown away.
            # As the fft spectrum is symmetrical, half of the spectrum is removed.
            # That's fine.
            # But the consequence is that half of the power is removed.
            # This is a mistake.

            #
            #
            # doppler domain
            # - first remove abs average
            # -- not done in the 2019 version
            # -- might lead to bad calibration due to windowing

            # - apply windowing
            this_scaled_window = get_scaled_window(
                n=n_dopfreq,
                window_name=dct_sp_settings["fft_dopplerdomain_windowing"])
            if this_scaled_window is not None:
                this_data = (
                    this_data * this_scaled_window[:, None])

            # - apply fft, and scale
            this_data = (
                np.fft.fft(this_data, axis=0) / n_dopfreq)

            # rename the variable
            new_name = "rangedoppler_" + channel_tag
            my_data_dct[new_name] = this_data

            del my_data_dct[myvar]


def get_scaled_window(
        n,
        i_start=None,
        i_end=None,
        window_name="Hamming",
        scaling_method="methodA",
        window_parameter1=None,
        verbose=True):
    """
    Support function to get a scaled window.

    For normal distributed variables:
    VAR(XY) = (si_x^2 + mu_x^2) (si_y^2 + mu_y^2) - (mu_x^2 mu_y^2)
    Assuming that mu_x = 0, we can write this as:
    VAR(XY) = (si_x^2) (si_y^2 + mu_y^2)

    Method A:

    Scaling of the terms is done,
    such that the variance of the new terms is practically equal.

    I.e. the compensating factor is:
    c_A = sqrt(1.0 / (si_y^2 + mu_y^2))

    Say, Z = c_A * X * Y, then
    VAR(Z) = c_A^2 * VAR(XY) = c_A^2 * (si_x^2) (si_y^2 + mu_y^2) = (si_x^2) = VAR(X)

    Method B:

    Scaling of the terms is done,
    with the variance of the window.
    Application of this window, has a result on the variance of the original variable.

    I.e. the compensating factor is:
    c_B = sqrt(1.0 / (si_y^2))

    Say, Z = c_B * X * Y, then
    VAR(Z) = c_B^2 * VAR(XY)
            = c_B^2 * (si_x^2) (si_y^2 + mu_y^2)
            = ((si_y^2 + mu_y^2) / (si_y^2)) * (si_x^2)
            = ((si_y^2 + mu_y^2) / (si_y^2)) VAR(X)            
    """

    if i_start is None:
        i_start = 0
    if i_end is None:
        i_end = n-1

    n_window = i_end - i_start + 1
    n_removed = n - n_window

    window_name_recognized = False
    # make the 1D-window and scale it

    if window_name == "Hamming":
        window = scipy.signal.windows.general_hamming(n_window, alpha=0.53836, sym=False)
        window_name_recognized = True
    if window_name == "Chebyshev":
        at_dB = 100
        window = scipy.signal.chebwin(n_window, at_dB, sym=False)
        window_name_recognized = True
    if window_name == "ConfinedGaussian":
        if window_parameter1 is None:
            sigma_t = 0.05
        else:
            sigma_t = window_parameter1
        window = confined_gaussian_window(n_window, sigma_t)
        window_name_recognized = True

    if not window_name_recognized:
        print("Warning: window name not recognized!")
        return None

    full_window = np.zeros(n)
    full_window[i_start:i_end+1] = window


    if scaling_method == "methodA":
        fct = np.sqrt(1.0 / np.mean(np.abs(full_window ** 2.)))
    if scaling_method == "methodB":
        fct = 1.0 / np.sqrt(np.mean(full_window))

    full_scaled_window = (
        full_window * fct)

    return full_scaled_window


def confined_gaussian_window(n, sigma_t=0.05):
    """Confined gaussian window. std is l * sigma_t."""

    # make a symmetric window
    n_h = int(n/2)
    ii = np.arange(n)
    ii[-1:-n_h-1:-1] = ii[:n_h]

    l = n + 1
    G = lambda x: np.exp(- (((x - n/2.) / (2. * l * sigma_t)) ** 2.))

    w = lambda x: G(x) - (
        G(-1/2.) * (G(x+l) + G(x-l))) / (
            G(-1/2. + l) + G(-1/2. - l))

    window = w(ii)
    return window



def do_fft_shift(
        dct_sp_result, dct_sp_settings):
    """Do fft shift in the doppler axis"""
    for my_key in ["data", "noise"]:
        if not my_key in dct_sp_result:
            continue

        my_data_dct = dct_sp_result[my_key]

        for channel_tag in radardata_channel_tags:
            myvar = "rangedoppler_" + channel_tag

            if myvar not in my_data_dct.keys():
                continue

            # - fft shift in the doppler axis
            my_data_dct[myvar] = np.fft.fftshift(my_data_dct[myvar], axes=0)


def do_apply_radar_constant(
        dct_sp_result, dct_sp_settings):
    """Apply the radar constant. This includes the radar calibration."""

    n_dopfreq = dct_sp_result["n_dopfreq"]
    n_range = dct_sp_result["n_range"]
    data_shape = (n_dopfreq, n_range)

    for my_key in ["data", "noise"]:
        if not my_key in dct_sp_result:
            continue

        my_data_dct = dct_sp_result[my_key]

        for channel_tag in radardata_channel_tags:
            myvar = "rangedoppler_" + channel_tag

            if myvar not in my_data_dct.keys():
                continue

            my_constant_var = "radar_constant_lin_" + channel_tag
            radar_constant_lin_sqrt = np.sqrt(my_data_dct[my_constant_var])

            # apply the radar constant
            new_data = np.empty(data_shape, dtype=np.cdouble)
            new_data[:, :n_range] = (
                my_data_dct[myvar][:, :n_range] *
                radar_constant_lin_sqrt[None, :])
            my_data_dct[myvar] = new_data


def do_calc_rangedoppler_reflectivity_linear(
        dct_sp_result, dct_sp_settings):
    """Calculate reflectivity linear."""
    for my_key in ["data", "noise"]:
        if not my_key in dct_sp_result:
            continue

        my_data_dct = dct_sp_result[my_key]

        for channel_tag in radardata_channel_tags:
            myvar = "rangedoppler_" + channel_tag

            if myvar not in my_data_dct.keys():
                continue

            # - fft shift in the doppler axis
            mynewvar = "rangedoppler_reflectivity_linear_" + channel_tag
            my_data_dct[mynewvar] = np.abs(my_data_dct[myvar]) ** 2.


def do_zerodoppler_clutterfilter(
        dct_sp_result, dct_sp_settings):
    """Do the zero doppler clutter filter."""
    if dct_sp_settings["zeroDopplerwidthfilter_width_ms"] <= 0:
        # nothing to do
        return

    data_shape = dct_sp_result["data_shape_2"]
    my_filter_velocities = (
        np.abs(dct_sp_result["doppler_velocity_ms"]) <
        dct_sp_settings["zeroDopplerwidthfilter_width_ms"])

    dct_sp_result["rangedoppler_filter_zeroDopplerwidth"] = \
        np.zeros(data_shape, dtype=bool)
    dct_sp_result["rangedoppler_filter_zeroDopplerwidth"][my_filter_velocities, :] = \
        True

    # apply it to the combined filter
    dct_sp_result["rangedoppler_filter_combined"] |= \
        dct_sp_result["rangedoppler_filter_zeroDopplerwidth"]


def do_set_noise_and_noise_clipping_levels(
        dct_sp_result, dct_sp_settings, method="version2021"):
    """Set noise and noise clipping levels"""
    if dct_sp_settings["noise_filtering"] == "from_measurement":
        do_set_noise_and_noise_clipping_levels_from_measurement(
            dct_sp_result, dct_sp_settings)

    if dct_sp_settings["noise_filtering"] == "from_spectra":
        do_set_noise_and_noise_clipping_levels_from_spectra(
            dct_sp_result, dct_sp_settings, method=method)


def do_set_noise_and_noise_clipping_levels_from_measurement(
        dct_sp_result, dct_sp_settings):
    """Set noise and noise clipping levels from measurement."""
    if not "noise" in dct_sp_result:
        if verbose:
            print(
                "Note: it's not possible to set noise levels from measurement,"
                " without a noise measurement.")
        return

    my_data_dct = dct_sp_result["noise"]

    for channel_tag in ["HH", "VV"]:
        myvar = "rangedoppler_reflectivity_linear_" + channel_tag

        if myvar not in my_data_dct.keys():
            continue

        # remove filtered data
        # assuming that this is only the zero Doppler width filter
        cond = (dct_sp_result["rangedoppler_filter_combined"][:, 0] == False)
        temp_data = my_data_dct[myvar][cond]

        # calculate noise_level_lin_
        noise_level_lin = np.average(
            temp_data,
            axis=0)

        # calculate noise_clipping_level_lin
        fct_lin = dB_inv(
            dct_sp_settings["noise_filtering_clipping_threshold_db"])
        noise_clipping_level_lin = fct_lin * noise_level_lin

        # add to dictionary
        mynewvar = "noise_level_lin_" + channel_tag
        dct_sp_result[mynewvar] = noise_level_lin

        mynewvar2 = "noise_clipping_level_lin_" + channel_tag
        dct_sp_result[mynewvar2] = noise_clipping_level_lin

    # apply noise filter
    my_data_dct = dct_sp_result["data"]
    for channel_tag in ["HH", "VV"]:
        myvar = "rangedoppler_reflectivity_linear_" + channel_tag

        if myvar not in my_data_dct.keys():
            continue

        # get the right noise clipping level
        noise_clipping_level_lin = dct_sp_result["noise_clipping_level_lin_" + channel_tag]

        filter_noiseclipping = (my_data_dct[myvar] <= noise_clipping_level_lin)

        # apply it to the combined filter
        dct_sp_result["rangedoppler_filter_combined"] |= \
            filter_noiseclipping

        # store the filter
        mynewvar = "rangedoppler_filter_noiseclipping_" + channel_tag
        dct_sp_result[mynewvar] = filter_noiseclipping


def do_set_noise_and_noise_clipping_levels_from_spectra(
        dct_sp_result, dct_sp_settings, method="version2021"):
    """Calculate noise and noise clipping levels."""

    n_dopfreq = dct_sp_result["n_dopfreq"]

    my_max_n_dopfreq = int(0.20 * n_dopfreq)
        # max doppler frequency that determines how much data is used

    my_data_dct = dct_sp_result["data"]
    for channel_tag in ["HH", "VV"]:
        myvar = "rangedoppler_reflectivity_linear_" + channel_tag

        if myvar not in my_data_dct.keys():
            continue

        # sort the reflectivity data (over the doppler axis)
        sorted_rd_refl = np.sort(my_data_dct[myvar], axis=0)

        if method == "version2021":
            # this method is assuming the sampling from a truncated normal distribution
            # for the lowest 20% of data.
            # see https://en.wikipedia.org/wiki/Truncated_normal_distribution .

            c_1, c_2 = 1.3998096020390423, 0.7813574379253931  # (coefficients for 20% data fraction)

            cond = np.arange(n_dopfreq) < my_max_n_dopfreq
            temp_data = sorted_rd_refl[cond]

            mom_1 = np.mean(temp_data, axis=0)
            mom_2 = np.mean(temp_data ** 2., axis=0)

            std_1 = np.sqrt(
                (mom_2 - (mom_1**2.))/
                (1 - c_2))
            mu_1 = mom_1 + std_1 * c_1

            # determine noise level lin
            noise_level_lin = mu_1

            # determine noise level variability
            noise_level_var = std_1

        if method == "version2020":
            # determine noise level lin
            noise_level_lin = sorted_rd_refl[my_max_n_dopfreq, :]

            # determine noise level variability
            cond = np.arange(n_dopfreq) < my_max_n_dopfreq
            temp_data = sorted_rd_refl[cond]
            noise_level_var = np.std(temp_data, axis=0)


        # determine noise clippling level
        noise_clipping_level_lin = (
            noise_level_lin + (
                dct_sp_settings["noise_filtering_clipping_threshold_std"] *
                noise_level_var))

        # now the filter
        filter_noiseclipping = (my_data_dct[myvar] <= noise_clipping_level_lin)

        # apply it to the combined filter
        dct_sp_result["rangedoppler_filter_combined"] |= \
            filter_noiseclipping

        # add to dictionary
        mynewvar = "noise_level_lin_" + channel_tag
        dct_sp_result[mynewvar] = noise_level_lin
        mynewvar = "noise_level_var_" + channel_tag
        dct_sp_result[mynewvar] = noise_level_var
        mynewvar = "noise_clipping_level_lin_" + channel_tag
        dct_sp_result[mynewvar] = noise_clipping_level_lin
        mynewvar = "rangedoppler_filter_noiseclipping_" + channel_tag
        dct_sp_result[mynewvar] = filter_noiseclipping


def do_calc_rangedoppler_correlation_coefficients(
        dct_sp_result, dct_sp_settings,
        apply_phase_correction=True):
    """Calculate correlation coefficients."""

    my_data_dct = dct_sp_result["data"]
    for i_calc in [1, 2, 3]:
        if i_calc == 1:
            varname_output = "rangedoppler_copolar_correlation"
            key_varname_rd_A = "rangedoppler_HH"
            key_varname_refl_A = "rangedoppler_reflectivity_linear_HH"
            key_varname_rd_B = "rangedoppler_VV"
            key_varname_refl_B = "rangedoppler_reflectivity_linear_VV"
            if apply_phase_correction:
                phase_correction = dct_sp_result["diffphase_comp_rad"][:, None]
            else:
                phase_correction = None

        if i_calc == 2:
            varname_output = "rangedoppler_cross_correlation_H"
            key_varname_rd_A = "rangedoppler_HH"
            key_varname_refl_A = "rangedoppler_reflectivity_linear_HH"
            key_varname_rd_B = "rangedoppler_HV"
            key_varname_refl_B = "rangedoppler_reflectivity_linear_HV"
            phase_correction = None

        if i_calc == 3:
            varname_output = "rangedoppler_cross_correlation_V"
            key_varname_rd_A = "rangedoppler_VV"
            key_varname_refl_A = "rangedoppler_reflectivity_linear_VV"
            key_varname_rd_B = "rangedoppler_VH"
            key_varname_refl_B = "rangedoppler_reflectivity_linear_VH"
            phase_correction = None

        if not key_varname_rd_A in my_data_dct:
            continue
        if not key_varname_rd_B in my_data_dct:
            continue

        rd_A = my_data_dct[key_varname_rd_A]
        rd_B = my_data_dct[key_varname_rd_B]
        refl_A = my_data_dct[key_varname_refl_A]
        refl_B = my_data_dct[key_varname_refl_B]

        if phase_correction is not None:
            rd_AB = rd_A * np.conjugate(rd_B * np.exp(1.j * phase_correction))
        else:
            rd_AB = rd_A * np.conjugate(rd_B)

        # do 2D-averaging
        av_rd_AB = averaging_2D(rd_AB, kernel_size=5)  # complex averaging (updated)
        av_refl_A = averaging_2D(refl_A, kernel_size=5)
        av_refl_B = averaging_2D(refl_B, kernel_size=5)

        # for the moment, take the average
        # -> in principle, for cross correlation, a complex correlation coefficient can be calculated.
        # -> See Bringi. (Eq. 6.16)
        av_rd_AB_abs = np.abs(av_rd_AB)

        # calculate the correlation
        correlation = (
            av_rd_AB_abs /
            np.sqrt(av_refl_A * av_refl_B))

        # store the result
        my_data_dct[varname_output] = correlation


def do_calc_rangedoppler_phase(
        dct_sp_result, dct_sp_settings,
        calc_smooth_diff_phase=True):
    """Calculate phase of the spectra. And also differential phase of the spectra."""

    my_data_dct = dct_sp_result["data"]

    # calculate rangedoppler_phase_
    for channel_tag in radardata_channel_tags:
        myvar = "rangedoppler_" + channel_tag

        if myvar not in my_data_dct.keys():
            continue

        new_var_name = "rangedoppler_phase_" + channel_tag
        phase_rad = np.angle(
            my_data_dct[myvar])
        my_data_dct[new_var_name] = phase_rad

    #
    #
    # differential phase calculation
    if (
            ("rangedoppler_phase_HH" in my_data_dct) and
            ("rangedoppler_phase_VV" in my_data_dct)):
        kPhaseoffset = 0.
        # - kPhaseoffset accounts for the phase offset between the transmitted and received channels.
        # ... TBD ... never determined.

        phase_rad_HH = my_data_dct["rangedoppler_phase_HH"]
        phase_rad_VV = my_data_dct["rangedoppler_phase_VV"]

        # calculate range doppler differential phase
        # without phase correction
        diff_phase_wo_phase_correction_rad = (
            phase_rad_HH - phase_rad_VV - kPhaseoffset)

        # calculate spectral differential phase compensation
        # eq. 4.58 from Jordis thesis
        diffphase_comp_rad_part1 = (
            -2. * np.pi * 2 * dct_sp_result["sweep_time_mus"] * 1.e-6 / dct_sp_result["wavel_m"])

        diffphase_comp_rad = dct_sp_result["doppler_velocity_ms"] * diffphase_comp_rad_part1

        # apply this apply correction
        diffphase_rad = (
            diff_phase_wo_phase_correction_rad +
            diffphase_comp_rad[:, None])

        # put it in [-pi, pi]
        diff_phase_wo_phase_correction_rad[
            diff_phase_wo_phase_correction_rad > np.pi] -= (2 * np.pi)
        diff_phase_wo_phase_correction_rad[
            diff_phase_wo_phase_correction_rad < -np.pi] += (2. * np.pi)
        diffphase_rad[diffphase_rad > np.pi] -= (2. * np.pi)
        diffphase_rad[diffphase_rad < -np.pi] += (2. * np.pi)

        # store it in the dct
        dct_sp_result["diffphase_comp_rad"] = diffphase_comp_rad
        my_data_dct["rangedoppler_diff_phase_wo_phase_correction_rad"] = diff_phase_wo_phase_correction_rad
        my_data_dct["rangedoppler_diff_phase_rad"] = diffphase_rad


    if ("rangedoppler_diff_phase_rad" in my_data_dct) and calc_smooth_diff_phase:
        rd_refl_lin = my_data_dct["rangedoppler_reflectivity_linear_HH"]
        rangedoppler_diff_phase_rad = my_data_dct["rangedoppler_diff_phase_rad"] = diffphase_rad

        diff_phase_rad_smooth_weighted_sum = averaging_2D(
            rd_refl_lin *
            np.exp(1.j * rangedoppler_diff_phase_rad), kernel_size=10)

        diff_phase_smooth_rad = np.angle(diff_phase_rad_smooth_weighted_sum)

        my_data_dct["rangedoppler_diff_phase_smooth_rad"] = diff_phase_smooth_rad


def do_sLDR_filter(
        dct_sp_result, dct_sp_settings):
    """Apply the sLDR filter."""

    my_data_dct = dct_sp_result["data"]
    for i_calc in [1, 2]:
        if i_calc == 1:
            # sLDR_HV_filter
            if not dct_sp_settings["sLDR_HV_filter"]:
                continue

            varname_1 = "rangedoppler_reflectivity_linear_HV"
            varname_2 = "rangedoppler_reflectivity_linear_HH"
            filter_treshold_dB = dct_sp_settings["sLDR_HV_filter_threshold_db"]
            filter_varname = "rangedoppler_filter_sLDR_HV"

        if i_calc == 2:
            # sLDR_VH_filter
            if not dct_sp_settings["sLDR_VH_filter"]:
                continue

            varname_1 = "rangedoppler_reflectivity_linear_VH"
            varname_2 = "rangedoppler_reflectivity_linear_VV"
            filter_treshold_dB = dct_sp_settings["sLDR_VH_filter_threshold_db"]
            filter_varname = "rangedoppler_filter_sLDR_VH"

        if not varname_1 in my_data_dct:
            continue
        if not varname_2 in my_data_dct:
            continue

        sLDR_dB = dB(
            my_data_dct[varname_1] /
            my_data_dct[varname_2])

        filter_sLDR = (
            sLDR_dB > filter_treshold_dB)

        # apply it to the combined filter
        dct_sp_result["rangedoppler_filter_combined"] |= \
            filter_sLDR

        # add to dictionary
        dct_sp_result[filter_varname] = filter_sLDR


def do_abs_sZDR_filter(
        dct_sp_result, dct_sp_settings):
    """Apply the abs sZDR filter."""

    if not dct_sp_settings["abs_sZDR_filter"]:
        return

    my_data_dct = dct_sp_result["data"]
    varname_1 = "rangedoppler_reflectivity_linear_HH"
    varname_2 = "rangedoppler_reflectivity_linear_VV"
    filter_treshold_dB = dct_sp_settings["abs_sZDR_filter_threshold_db"]
    filter_varname = "rangedoppler_filter_abs_sZDR"

    sZDR_abs_dB = np.abs(dB(
        my_data_dct[varname_1] /
        my_data_dct[varname_2]))

    filter_sZDR_abs = (
        sZDR_abs_dB > filter_treshold_dB)

    # apply it to the combined filter
    dct_sp_result["rangedoppler_filter_combined"] |= \
        filter_sZDR_abs

    # add to dictionary
    dct_sp_result[filter_varname] = filter_sZDR_abs


def do_copolar_correlation_filter(
        dct_sp_result, dct_sp_settings, dev=False):
    """Do the copolar correlation filter."""
    if not dct_sp_settings["copolar_correlation_filter"]:
        return

    automatic_threshold = False
    filter_treshold = 0.7
    if "copolar_correlation_filter_automatic_threshold" in dct_sp_settings:
        automatic_threshold = dct_sp_settings["copolar_correlation_filter_automatic_threshold"]

    if "copolar_correlation_filter_threshold" in dct_sp_settings:
        filter_treshold = dct_sp_settings["copolar_correlation_filter_threshold"]

    if automatic_threshold:
        # nothing to do
        return 
        
    filter_copolar_correlation = (
        my_data_dct["rangedoppler_copolar_correlation"] < filter_treshold)

    # apply it to the combined filter
    dct_sp_result["rangedoppler_filter_combined"] |= \
        filter_copolar_correlation

    # add to dictionary
    dct_sp_result["rangedoppler_filter_copolar_correlation"] = filter_copolar_correlation


def do_adaptive_copolar_correlation_filter(
        dct_sp_result, dct_sp_settings, dev=False):
    """Do the adaptive copolar correlation filter."""
    if not dct_sp_settings["copolar_correlation_filter"]:
        return

    automatic_threshold = False
    if "copolar_correlation_filter_automatic_threshold" in dct_sp_settings:
        automatic_threshold = dct_sp_settings["copolar_correlation_filter_automatic_threshold"]

    if not automatic_threshold:
        return

    my_data_dct = dct_sp_result["data"]
    rd_cop_corr = my_data_dct["rangedoppler_copolar_correlation"]
    n_dopfreq = dct_sp_result["n_dopfreq"]
    n_range = dct_sp_result["n_range"]

    # reorganize the data
        # this reshape puts together
        # data for range index [0:25], [25:50], etc. etc.
    alt_range_step = 25
    alt_n_range = int(n_range / alt_range_step)
    alt_i_range_0 = n_range - (alt_n_range * alt_range_step)
    alt_cc = rd_cop_corr[:, alt_i_range_0:].reshape(
        alt_range_step*n_dopfreq, alt_n_range, order="C")

    # take different variable
    f_theta = lambda cc: np.pi - np.arccos(cc)
    f_cc = lambda theta: np.cos(np.pi - theta)
    alt_theta_cc = f_theta(alt_cc)

    # sort it
    sorted_alt_theta_cc = np.sort(alt_theta_cc, axis=0)

    # make a subset that is assumed to only contains noise
    # use lowest <x>% of the sorted data
    max_n_ax0 = int(0.30 * sorted_alt_theta_cc.shape[0])
    alt_theta_cc_noise_subset = sorted_alt_theta_cc[:max_n_ax0, :]
        # determine index to  frequency that determines how much data is used

    # step 1: estimate the distribution parameters for noise theta_cc
    # - using truncated normal distribution estimation of parameters
    # - these numbers have to match when the selected data percentage
    # c_1, c_2 = 0.9658563337421513, 0.6881815532308918  # (coefficients for 40% data fraction)
    c_1, c_2 = 1.158975380666913, 0.7354566491542899  # (coefficients for 30% data fraction)
    # c_1, c_2 = 1.3998096020390423, 0.7813574379253931  # (coefficients for 20% data fraction)

    mom_1 = np.mean(alt_theta_cc_noise_subset, axis=0)
    mom_2 = np.mean(alt_theta_cc_noise_subset ** 2., axis=0)
    noise_theta_cc_std = np.sqrt(
        (mom_2 - (mom_1**2.))/
        (1 - c_2))
    noise_theta_cc_mu = mom_1 + noise_theta_cc_std * c_1

    # step 2: make histograms of the noise distribution
    n_bins = 100
    n_bin_edges = n_bins + 1
    hist_bin_edges = np.linspace(0, np.pi, n_bin_edges)

    hist_theta_cc = np.empty((n_bins, alt_n_range))
    for i_alt_range in range(alt_n_range):
        hist_theta_cc[:, i_alt_range] = np.histogram(
            alt_theta_cc[:, i_alt_range], bins=hist_bin_edges)[0]

    # step 3: calculate theoretical noise distribution
    hist_mod_theta_cc = np.empty((n_bins, alt_n_range))
    for i_alt_range in range(alt_n_range):
        cdf_vals = scipy.stats.norm.cdf(
            hist_bin_edges, loc=noise_theta_cc_mu[i_alt_range],
            scale=noise_theta_cc_std[i_alt_range])
        hist_mod_theta_cc[:, i_alt_range] = np.diff(cdf_vals)

    # step 4: do tests with jensen shanon
    # and estimate cc threshold
    theta_cc_auto_threshold = np.empty(alt_n_range)

    for i_alt_range in range(alt_n_range):
        # test with jensen shanon
        lst_dat_1 = []
        lst_dat_2 = []
        lst_theta_cc = []
        for i_1 in range(70, n_bins - 1):
            hist_cc_before = hist_theta_cc[:i_1, i_alt_range]
            hist_cc_after = hist_theta_cc[i_1:, i_alt_range]
            hist_cc_mod_before = hist_mod_theta_cc[:i_1, i_alt_range]
            hist_cc_mod_after = hist_mod_theta_cc[i_1:, i_alt_range]

            test_1 = scipy.spatial.distance.jensenshannon(
                hist_cc_before,
                hist_cc_mod_before)
            test_2 = scipy.spatial.distance.jensenshannon(
                hist_cc_after,
                hist_cc_mod_after)

            lst_theta_cc.append(hist_bin_edges[i_1])
            lst_dat_1.append(test_1)
            lst_dat_2.append(test_2)

        lst_dat_3 = np.array(lst_dat_2) - np.array(lst_dat_1)

        i_argmax = np.argmax(lst_dat_3)
        theta_cc_auto_threshold[i_alt_range] = lst_theta_cc[i_argmax]

    cc_auto_threshold = f_cc(theta_cc_auto_threshold)
    # set min and max values
    cc_auto_threshold[cc_auto_threshold < 0.70] = 0.70
    cc_auto_threshold[cc_auto_threshold > 0.98] = 0.98

    # convert back to actual range
    cc_auto_threshold_new = np.empty(n_range)
    for i in range(alt_i_range_0):
        cc_auto_threshold_new[i] = cc_auto_threshold[0]
    for i in range(alt_range_step):
        cc_auto_threshold_new[alt_i_range_0 + i:n_range:alt_range_step] = cc_auto_threshold[:]
    cc_auto_threshold = cc_auto_threshold_new

    # now apply the filter
    filter_copolar_correlation = (
        rd_cop_corr < cc_auto_threshold[None, :])

    # add to dictionary
    dct_sp_result["moments"]["copolar_correlation_adaptive_threshold"] = cc_auto_threshold

    # apply it to the combined filter
    dct_sp_result["rangedoppler_filter_combined"] |= \
        filter_copolar_correlation

    # add to dictionary
    dct_sp_result["rangedoppler_filter_copolar_correlation"] = filter_copolar_correlation


def do_speckle_filter(
        dct_sp_result, dct_sp_settings, kernel_size=3):
    """Do the speckle filtering.
        - input: combined filtering efforts so far.
        - output: filter that indicates what has been fitlered due to speckle."""

    if not dct_sp_settings["speckle_filter"]:
        return

    filter_varname = "rangedoppler_filter_speckle"
    filter_valid_data = (
        dct_sp_result["rangedoppler_filter_combined"] == False)

    filter_speckle = basic_2D_speckle_filter(
        filter_valid_data,
        kernel_size=kernel_size)

    # apply it to the combined filter
    dct_sp_result["rangedoppler_filter_combined"] |= \
        filter_speckle

    # add to dictionary
    dct_sp_result[filter_varname] = filter_speckle


def do_doppler_dealiasing(
        dct_sp_result, dct_sp_settings,):
    """Doppler dealiasing calculations."""

    # doppler_velocity_ms has values in the domain [-vmax,+vmax]
    # however, the velocities could be anything [-vmax,+vmax] + i_fold * v_tot
    # here i_fold is determined

    my_data_dct = dct_sp_result["data"]
    n_range = dct_sp_result["n_range"]
    n_dopfreq = dct_sp_result["n_dopfreq"]

    doppler_velocity_ms = dct_sp_result["doppler_velocity_ms"]
    vmax_ms = dct_sp_result["doppler_velocity_max_ms"]

    rd_i_fold = np.zeros((n_dopfreq, n_range), dtype=int)

    use_differential_phase = False
    # - by default differential phase usage is off

    if (("doppler_dealiasing_use_differential_phase" in dct_sp_settings) and
            dct_sp_settings["doppler_dealiasing_use_differential_phase"]):
        use_differential_phase = True

    differential_phase_used = False
    if use_differential_phase and ("rangedoppler_diff_phase_smooth_rad" in my_data_dct):
        # dealise based on dsmooth ifferential phase
        # criterian for when to use the alternative doppler velocity

        # at the moment, this option gives unstable results in the Doppler spectra
        # and therefore it is not used.

        rangedoppler_diff_phase_smooth_rad = my_data_dct["rangedoppler_diff_phase_smooth_rad"]

        use_alt_doppler_velocity = (
            np.abs(rangedoppler_diff_phase_smooth_rad) > (np.pi/2.) * np.ones((n_dopfreq, n_range)))

        temp_rd_i_alt_fold = np.where(
            (np.ones((n_dopfreq, n_range)) * doppler_velocity_ms[:, None]) > 0.,
            -2,
            2)

        rd_i_fold[use_alt_doppler_velocity] = (
            temp_rd_i_alt_fold[use_alt_doppler_velocity])

        differential_phase_used = True

    doppler_velocity_ms_unfolded = (
        doppler_velocity_ms[:, None] + rd_i_fold * vmax_ms)

    # calculate HH velocity
    channel_tag = "HH"
    rd_refl_lin = my_data_dct["rangedoppler_reflectivity_linear_" + channel_tag]
    reflectivity_lin = np.sum(
        (dct_sp_result["rangedoppler_filter_combined"] == False) * rd_refl_lin,
        axis=0)
    calc1_velocity_ms = (
        np.sum(
            (dct_sp_result["rangedoppler_filter_combined"] == False) *
            rd_refl_lin *
            doppler_velocity_ms_unfolded,
            axis=0)) / reflectivity_lin

    # now center the velocities around this one
    if differential_phase_used:
        rd_i_fold = np.where(
            (doppler_velocity_ms_unfolded - calc1_velocity_ms[None, :]) > (2 * vmax_ms),
            rd_i_fold - 4,
            rd_i_fold)
        rd_i_fold = np.where(
            (doppler_velocity_ms_unfolded - calc1_velocity_ms[None, :]) < (-2 * vmax_ms),
            rd_i_fold + 4,
            rd_i_fold)
    else:
        rd_i_fold = np.where(
            (doppler_velocity_ms_unfolded - calc1_velocity_ms[None, :]) > vmax_ms,
            rd_i_fold - 2,
            rd_i_fold)
        rd_i_fold = np.where(
            (doppler_velocity_ms_unfolded - calc1_velocity_ms[None, :]) < -vmax_ms,
            rd_i_fold + 2,
            rd_i_fold)

    dct_sp_result["rd_dealiasing_i_fold"] = rd_i_fold


def do_calc_spectral_quality_index_v2020(
        dct_sp_result, dct_sp_settings):
    """Calculate spectral quality index. Depracated."""

    n_range = dct_sp_result["n_range"]
    n_dopfreq = dct_sp_result["n_dopfreq"]

    if "moments" in dct_sp_result:
        dct_moments = dct_sp_result["moments"]
    else:
        dct_moments = {}
        dct_sp_result["moments"] = dct_moments

    pol_filter_max_value = (
        (1 * dct_sp_settings["sLDR_HV_filter"]) +
        (1 * dct_sp_settings["sLDR_VH_filter"]) +
        (1 * dct_sp_settings["abs_sZDR_filter"]) +
        (1 * dct_sp_settings["copolar_correlation_filter"]))

    rd_filter_count = np.zeros((n_dopfreq, n_range))

    for filter_name in [
            "rangedoppler_filter_sLDR_HV",
            "rangedoppler_filter_sLDR_VH",
            "rangedoppler_filter_abs_sZDR",
            "rangedoppler_filter_copolar_correlation",]:
        if not filter_name in dct_sp_result:
            continue

        my_filter = dct_sp_result[filter_name]
        rd_filter_count += (my_filter)

    # remove results for the following filters
    for filter_name in [
            "rangedoppler_filter_noiseclipping_HH",
            "rangedoppler_filter_noiseclipping_VV",
            "rangedoppler_filter_zeroDopplerwidth",
        ]:
        if not filter_name in dct_sp_result:
            continue

        my_filter = dct_sp_result[filter_name]
        rd_filter_count[my_filter] = -1

    # remove pixels that are not filtered by abs sZDR
    filter_name = "rangedoppler_filter_abs_sZDR"
    if filter_name in dct_sp_result:
        my_filter = dct_sp_result[filter_name]
        rd_filter_count[my_filter == False] = -1

    filter_inconsistent = np.sum(rd_filter_count == 1, axis=0)
    filter_consistent = np.sum(rd_filter_count == pol_filter_max_value, axis=0)
    spectral_quality_index = (
        (1. * filter_consistent) /
        (1. * filter_consistent + filter_inconsistent))

    spectral_quality_index[~np.isfinite(spectral_quality_index)] = 1.0

    dct_moments["spectral_quality_index"] = spectral_quality_index
    dct_sp_result["moments"] = dct_moments

def do_moments_calculation(
        dct_sp_result, dct_sp_settings):
    """Calculate radar moments."""

    my_data_dct = dct_sp_result["data"]
    n_dopfreq = dct_sp_result["n_dopfreq"]
    my_data_dct = dct_sp_result["data"]
    doppler_velocity_ms = dct_sp_result["doppler_velocity_ms"]
    vmax_ms = dct_sp_result["doppler_velocity_max_ms"]

    if "moments" in dct_sp_result:
        dct_moments = dct_sp_result["moments"]
    else:
        dct_moments = {}
        dct_sp_result["moments"] = dct_moments

    # dictionary with radar moments

    # unfold doppler velocities
    if "rd_dealiasing_i_fold" in dct_sp_result:
        rd_dealiasing_i_fold = dct_sp_result["rd_dealiasing_i_fold"]
        doppler_velocity_ms_unfolded = (
            doppler_velocity_ms[:, None] + rd_dealiasing_i_fold * vmax_ms)
    else:
        doppler_velocity_ms_unfolded = doppler_velocity_ms[:, None]

    # further calculations
    for channel_tag in radardata_channel_tags:
        rd_refl_lin = my_data_dct["rangedoppler_reflectivity_linear_" + channel_tag]
        reflectivity_lin = np.sum(
            (dct_sp_result["rangedoppler_filter_combined"] == False) * rd_refl_lin,
            axis=0)

        # store it
        dct_moments["reflectivity_dB_"+channel_tag] = dB(reflectivity_lin)

        # calculation of velocity and width
        dct_moments["velocity_ms"] = (
            np.sum(
                (dct_sp_result["rangedoppler_filter_combined"] == False) *
                rd_refl_lin *
                doppler_velocity_ms_unfolded,
                axis=0)) / reflectivity_lin

        m2 = \
            np.sum(
                (dct_sp_result["rangedoppler_filter_combined"] == False) *
                rd_refl_lin *
                (doppler_velocity_ms_unfolded ** 2.),
                axis=0) / reflectivity_lin
        dct_moments["width_ms"] = (
            np.sqrt(m2 - (dct_moments["velocity_ms"] ** 2.)))

    # calculation of phidp_rad
    if "rangedoppler_diff_phase_rad" in my_data_dct:
        rangedoppler_diff_phase_rad = my_data_dct["rangedoppler_diff_phase_rad"]
        # NOTE: correction for dual transmission is already applied
        rd_refl_lin = my_data_dct["rangedoppler_reflectivity_linear_HH"]

        phidp_weighted_sum = \
            np.sum(
                (dct_sp_result["rangedoppler_filter_combined"] == False) *
                rd_refl_lin *
                np.exp(1.j * rangedoppler_diff_phase_rad),
                axis=0)

        # finalization of velocity and width
        dct_moments["PhiDP_rad"] = np.angle(phidp_weighted_sum)

    # calculate polarimetric variables
    dct_moments["ZDR_dB"] = (
        dct_moments["reflectivity_dB_HH"] -
        dct_moments["reflectivity_dB_VV"])
    dct_moments["LDR_HV_dB"] = (
        dct_moments["reflectivity_dB_HV"] -
        dct_moments["reflectivity_dB_HH"])
    dct_moments["LDR_VH_dB"] = (
        dct_moments["reflectivity_dB_VH"] -
        dct_moments["reflectivity_dB_VV"])

    # correlation coefficients
    for i_calc in [1, 2, 3]:
        if i_calc == 1:
            varname_output = "copolar_correlation"
            key_varname_rd_A = "rangedoppler_HH"
            key_varname_refl_A = "rangedoppler_reflectivity_linear_HH"
            key_varname_rd_B = "rangedoppler_VV"
            key_varname_refl_B = "rangedoppler_reflectivity_linear_VV"
            phase_correction = dct_sp_result["diffphase_comp_rad"][:, None]

        if i_calc == 2:
            varname_output = "cross_correlation_H"
            key_varname_rd_A = "rangedoppler_HH"
            key_varname_refl_A = "rangedoppler_reflectivity_linear_HH"
            key_varname_rd_B = "rangedoppler_HV"
            key_varname_refl_B = "rangedoppler_reflectivity_linear_HV"
            phase_correction = None

        if i_calc == 3:
            varname_output = "cross_correlation_V"
            key_varname_rd_A = "rangedoppler_VV"
            key_varname_refl_A = "rangedoppler_reflectivity_linear_VV"
            key_varname_rd_B = "rangedoppler_VH"
            key_varname_refl_B = "rangedoppler_reflectivity_linear_VH"
            phase_correction = None

        if not key_varname_rd_A in my_data_dct:
            continue
        if not key_varname_rd_B in my_data_dct:
            continue

        rd_A = my_data_dct[key_varname_rd_A]
        rd_B = my_data_dct[key_varname_rd_B]
        refl_A = my_data_dct[key_varname_refl_A]
        refl_B = my_data_dct[key_varname_refl_B]

        rd_AB = rd_A * np.conjugate(rd_B)
        if phase_correction is not None:
            rd_AB *= np.exp(1.j * phase_correction)

        # do averaging (in the Doppler axis)
        av_rd_abs_AB = np.mean(
            (dct_sp_result["rangedoppler_filter_combined"] == False) * rd_AB, axis=0)
        av_refl_A = np.mean(
            (dct_sp_result["rangedoppler_filter_combined"] == False) * refl_A, axis=0)
        av_refl_B = np.mean(
            (dct_sp_result["rangedoppler_filter_combined"] == False) * refl_B, axis=0)

        # calculate the correlation
        correlation = (
            av_rd_abs_AB /
            np.sqrt(av_refl_A * av_refl_B))

        if i_calc == 1:
            # - in case of copolar correlation, take abs
            correlation = np.abs(correlation)

        # store the result
        dct_moments[varname_output] = correlation

    # noise levels, and SNR
    for channel_tag in ["HH", "VV"]:
        noise_level_lin = dct_sp_result["noise_level_lin_"+channel_tag]
        reflectivity_dBZ = dct_moments["reflectivity_dB_"+channel_tag]

        noise_level_dBZ = dB(noise_level_lin)
        SNR_dB = reflectivity_dBZ - noise_level_dBZ

        dct_moments["noise_level_dBZ_"+channel_tag] = noise_level_dBZ
        dct_moments["SNR_dB_"+channel_tag] = SNR_dB

    dct_sp_result["moments"] = dct_moments


def do_data_discarding(
        dct_sp_result, dct_sp_settings):
    """Do data discarding."""

    global radardata_channel_tags

    n_range = dct_sp_result["n_range"]
    n_dopfreq = dct_sp_result["n_dopfreq"]
    data_discarded = np.zeros(n_range, dtype=bool)
    my_data_dct = dct_sp_result["data"]
    dct_moments = dct_sp_result["moments"]

    #
    #
    # discard cells with little doppler bins
    if (
            ("discard_cells_with_little_doppler_bins" in dct_sp_settings) and
            (dct_sp_settings["discard_cells_with_little_doppler_bins"])):
        # calculate percentage of the Doppler spectrum
        # that survives this filtering
        percentage_filtered = (
            100. *
            np.sum(dct_sp_result["rangedoppler_filter_combined"], axis=0) /
            n_dopfreq)
        cells_with_little_doppler_bins = (
            percentage_filtered <
            dct_sp_settings["discard_cells_with_little_doppler_bins_percentage"])

        # add to dct
        dct_sp_result["percentage_filtered"] = percentage_filtered
        dct_sp_result["cells_with_little_doppler_bins"] = cells_with_little_doppler_bins

        # combine with discard data
        data_discarded |= cells_with_little_doppler_bins


    if (
            ("discard_saturated_data" in dct_sp_settings) and
            (dct_sp_settings["discard_saturated_data"])):
        data_is_saturated = False
        for channel_tag in radardata_channel_tags:
            my_var_name = "saturation_percentage_" + channel_tag

            if (my_data_dct[my_var_name] >
                    dct_sp_settings["discard_saturated_data_treshold_percentage"]):
                data_is_saturated = True

        # add to dct
        dct_sp_result["data_is_saturated"] = data_is_saturated

        # combine with discard data
        data_discarded |= data_is_saturated

    if (
            ("discard_cells_with_low_copolar_correlation" in dct_sp_settings) and
            (dct_sp_settings["discard_cells_with_low_copolar_correlation"])):
        low_cc = (
            dct_moments["copolar_correlation"] <
            dct_sp_settings["discard_cells_with_low_copolar_correlation_threshold"])

        # combine with discard data
        data_discarded |= low_cc

    if (
            ("discard_bad_spectral_quality_index" in dct_sp_settings) and
            (dct_sp_settings["discard_bad_spectral_quality_index"])):
        low_quality = (
            dct_moments["spectral_quality_index"] <
            dct_sp_settings["discard_bad_spectral_quality_index_treshold"])

        # combine with discard data
        data_discarded |= low_quality


    if (
            ("discard_cells_with_high_LDR" in dct_sp_settings) and
            (dct_sp_settings["discard_cells_with_high_LDR"])):
        high_LDR = (
            dct_moments["LDR_HV_dB"] >
            dct_sp_settings["discard_cells_with_high_LDR_threshold"])
        high_LDR |= (
            dct_moments["LDR_VH_dB"] >
            dct_sp_settings["discard_cells_with_high_LDR_threshold"])

        # combine with discard data
        data_discarded |= high_LDR

    # apply it to moments
    dct_moments = dct_sp_result["moments"]

    for myvar in dct_moments.keys():
        dct_moments[myvar][data_discarded] = np.nan

    dct_sp_result["data_discarded"] = data_discarded


def create_mcl1_data_stats(
        sl1_filename,
        n_sweep_truncation=None,):
    """Create dictionary with some convenient level1 data statistics."""

    global user_filename
    global radardata_channel_tags

    # walk through sl1 data
    my_reader = level1_io.mcl1_reader(
        sl1_filename)
    n_dopfreq = 1

    # set n_sweep_truncation
    if n_sweep_truncation is None:
        n_sweep_truncation = 8

    # create empty dct for results
    mcl1_data_stats = {}

    while True:
        mcl1_data = my_reader.get_mcl1_data(n_dopfreq=n_dopfreq)
        if mcl1_data is None:
            break

        instrument_name = mcl1_data["instrument_name"]
        n_samples = mcl1_data["n_samples"]

        # data saturation
        for channel_tag in radardata_channel_tags:
            my_var_name = "saturation_percentage_" + channel_tag

            saturation_threshold = (0.99 * mcl1_data["max_abs_value_"+channel_tag])
            rawdata_linear = mcl1_data["rawdata_linear_"+channel_tag][n_sweep_truncation:]
            n_points = n_samples - n_sweep_truncation

            value = (
                100. * np.sum(np.abs(rawdata_linear) > saturation_threshold) /
                (1. * n_points))

            # create the var
            if not my_var_name in mcl1_data_stats:
                mcl1_data_stats[my_var_name] = []

            # add the value
            mcl1_data_stats[my_var_name].append(value)

        # rx power
        for channel_tag in radardata_channel_tags:
            my_var_name = "receiver_pow_dBm_" + channel_tag

            rawdata_linear = mcl1_data["rawdata_linear_"+channel_tag][n_sweep_truncation:]
            n_points = n_samples - n_sweep_truncation

            receiver_pow_W = np.abs(np.var(rawdata_linear))
            receiver_pow_dBm = level1_io.dB(receiver_pow_W * 1.e3)

            # create the var
            if not my_var_name in mcl1_data_stats:
                mcl1_data_stats[my_var_name] = []

            # add the value
            mcl1_data_stats[my_var_name].append(receiver_pow_dBm)

        # take over some essential variables
        for my_var_name in [
                "utc_unixtimestamp",
                "antenna_azimuth_angle_rad",
                "antenna_elevation_angle_rad",
                "transmit_pow_W",
                "attenuation_H_dB",
                "attenuation_V_dB",
                ]:
            # create the var
            if not my_var_name in mcl1_data_stats:
                mcl1_data_stats[my_var_name] = []

            value = mcl1_data[my_var_name]
            # add the value
            mcl1_data_stats[my_var_name].append(value)

    # set instrument name
    mcl1_data_stats["instrument_name"] = instrument_name


    return mcl1_data_stats


all_rd_keys = [
    "rangedoppler_copolar_correlation",
    "rangedoppler_diff_phase_rad",
    "rangedoppler_diff_phase_smooth_rad",
    "rangedoppler_diff_phase_wo_phase_correction_rad",
    "rangedoppler_filter_abs_sZDR",
    "rangedoppler_filter_combined",
    "rangedoppler_filter_copolar_correlation",
    "rangedoppler_cross_correlation_H",
    "rangedoppler_cross_correlation_V",
    "rangedoppler_filter_noiseclipping_HH",
    "rangedoppler_filter_noiseclipping_VV",
    "rangedoppler_filter_sLDR_HV",
    "rangedoppler_filter_sLDR_VH",
    "rangedoppler_filter_speckle",
    "rangedoppler_filter_zeroDopplerwidth",
    "rangedoppler_phase_HH",
    "rangedoppler_phase_HV",
    "rangedoppler_phase_VH",
    "rangedoppler_phase_VV",
    "rangedoppler_reflectivity_HH",
    "rangedoppler_reflectivity_HV",
    "rangedoppler_reflectivity_VH",
    "rangedoppler_reflectivity_VV",
    "rangedoppler_sLDR_dB_HV",
    "rangedoppler_sLDR_dB_VH",
    "rangedoppler_sZDR_dB",
]


def get_dct_plot_rd_var(
        dct_sp_result,
        plot_variable="doppler_reflectivity_HH",):
    """Support function for plotting data.
        Return dictionary with plotting variables,
        used for doppler and rangedoppler plots."""

    n_range = dct_sp_result["n_range"]
    n_dopfreq = dct_sp_result["n_dopfreq"]
    vmax_ms = dct_sp_result["doppler_velocity_max_ms"]

    my_data_dct = dct_sp_result["data"]

    cbar_lab = None
    title = None
    my_MaxNLocator = None
    my_data = None
    my_val_min = None
    my_val_max = None
    my_cmap = None

    if "rd_dealiasing_i_fold" in dct_sp_result:
        rd_dealiasing_i_fold = dct_sp_result["rd_dealiasing_i_fold"]
    else:
        rd_dealiasing_i_fold = None

    #
    #
    # get data
    if plot_variable in [
            "rangedoppler_reflectivity_HH",
            "rangedoppler_reflectivity_HV",
            "rangedoppler_reflectivity_VH",
            "rangedoppler_reflectivity_VV"]:
        # - (to get dBZ, we have to multiply with n_dopfreq)
        my_pol = plot_variable[-2:]
        my_data = dB(n_dopfreq * dct_sp_result["data"]["rangedoppler_reflectivity_linear_"+my_pol])

    if plot_variable in [
            "rangedoppler_sLDR_dB_HV",
            "rangedoppler_sLDR_dB_VH",
            "rangedoppler_sZDR_dB"]:
        if plot_variable == "rangedoppler_sLDR_dB_HV":
            my_pol_A = "HV"
            my_pol_B = "HH"

        if plot_variable == "rangedoppler_sLDR_dB_VH":
            my_pol_A = "VH"
            my_pol_B = "VV"

        if plot_variable == "rangedoppler_sZDR_dB":
            my_pol_A = "HH"
            my_pol_B = "VV"

        my_data_A = dB(dct_sp_result["data"]["rangedoppler_reflectivity_linear_"+my_pol_A])
        my_data_B = dB(dct_sp_result["data"]["rangedoppler_reflectivity_linear_"+my_pol_B])
        my_data = my_data_A - my_data_B

    # get the data
    if my_data is None:
        if plot_variable in dct_sp_result:
            my_data = dct_sp_result[plot_variable]
        if plot_variable in my_data_dct:
            my_data = my_data_dct[plot_variable]

    if my_data is None:
        print("Variable not available.")
        return None

    #
    #
    # further specific settings
    if plot_variable in [
            "rangedoppler_reflectivity_HH",
            "rangedoppler_reflectivity_HV",
            "rangedoppler_reflectivity_VH",
            "rangedoppler_reflectivity_VV"]:
        my_pol = plot_variable[-2:]
        my_val_min = -20.
        my_val_max = 60.
        cbar_lab = "Reflectivity [dBZ]"
        title = "Spectral reflectivity " + my_pol

    if "sLDR" in plot_variable:
        my_pol = plot_variable[-2:]
        my_val_min = -30.
        my_val_max = 5.
        cbar_lab = "sLDR [dB]"
        title = "sLDR " + my_pol

    if "sZDR" in plot_variable:
        my_val_min = -10.
        my_val_max = 10.
        cbar_lab = "sZDR [dB]"
        title = "sZDR"

    if plot_variable in [
            "rangedoppler_copolar_correlation",
            "rangedoppler_cross_correlation_H",
            "rangedoppler_cross_correlation_V",]:
        my_val_min = 0.
        my_val_max = 1.
        cbar_lab = "Correlation [-]"
        
        if "cross" in plot_variable:
            my_data = np.abs(my_data)
            cbar_lab = "Correlation magnitude [-]"

        title = plot_variable[13:].replace("_", " ")

    if plot_variable in [
            "rangedoppler_phase_HH",
            "rangedoppler_phase_HV",
            "rangedoppler_phase_VH",
            "rangedoppler_phase_VV",
            "rangedoppler_diff_phase_wo_phase_correction_rad",
            "rangedoppler_diff_phase_rad",
            "rangedoppler_diff_phase_smooth_rad",
            ]:
        my_val_min = - np.pi
        my_val_max = np.pi
        my_cmap = plt.cm.bwr

        if "diff_phase" in plot_variable:
            cbar_lab = "Differential phase [rad]"
        else:
            cbar_lab = "Phase [rad]"
        title = plot_variable[13:].replace("_", " ").replace("rad", "")

    if "filter" in plot_variable:
        title = (
            plot_variable.replace("rangedoppler_filter", " ").replace("_", " ") +
            " filter")
        my_cmap = matplotlib.colors.ListedColormap(
            plt.cm.binary_r([0, 1.0]))
        my_val_min = 0.
        my_val_max = 1.

    # other
    if my_val_min is None:
        my_val_min = np.min(my_data)
    if my_val_max is None:
        my_val_max = np.max(my_data)
    if my_cmap is None:
        my_cmap = plt.cm.jet

    # unravel doppler folding
    # make a mapping function
    if rd_dealiasing_i_fold is not None:

        i_fold_min = np.min(rd_dealiasing_i_fold)
        i_fold_max = np.max(rd_dealiasing_i_fold)
        i_fold_delta = i_fold_max - i_fold_min + 1

        spectrum_data = np.zeros((i_fold_delta * n_dopfreq, n_range)) + np.nan

        my_map_row, my_map_col = np.indices((n_dopfreq, n_range))
        my_row_offset = int(-i_fold_min * n_dopfreq)
        my_map_row += my_row_offset + (rd_dealiasing_i_fold * int(n_dopfreq / 2))
        spectrum_data[my_map_row, my_map_col] = my_data[:]

        spec_vmin = dct_sp_result["doppler_velocity_ms"][0] + (i_fold_min * vmax_ms)
        spec_vmax = dct_sp_result["doppler_velocity_ms"][-1] + (i_fold_max * vmax_ms)

        vmax_lines = np.arange(i_fold_min + ((i_fold_min % 2) == 0), i_fold_max + 1, 2) * vmax_ms
    else:
        spectrum_data = my_data
        spec_vmin = dct_sp_result["doppler_velocity_ms"][0]
        spec_vmax = dct_sp_result["doppler_velocity_ms"][-1]
        vmax_lines = []


    my_cmap.set_bad(color="black")

    dct_plot_rd_var = {
        "n_range":  n_range,
        "n_dopfreq":  n_dopfreq,
        "vmax_ms":  vmax_ms,

        "cbar_lab":  cbar_lab,
        "title":  title,
        "my_MaxNLocator": my_MaxNLocator,

        "spectrum_data": spectrum_data,

        "my_val_min": my_val_min,
        "my_val_max": my_val_max,

        "my_cmap": my_cmap,

        "vmax_lines": vmax_lines,

        "spec_vmin": spec_vmin,
        "spec_vmax": spec_vmax,
        "range_km_min": 1.e-3 * dct_sp_result["range_m"][0],
        "range_km_max": 1.e-3 * dct_sp_result["range_m"][-1],
    }

    return dct_plot_rd_var


def plot_doppler(
        dct_sp_result,
        dct_sp_settings,
        plot_variable="doppler_reflectivity_HH",
        range_index=None,
        root_folder=skyrapy_plot_dir,
        folder_tag="level1_quicklooks",
        file_tag="level1_quicklooks",
        verbose=True):
    """Plot one-dimensional Doppler spectrum for a specific range."""

    n_range = dct_sp_result["n_range"]
    n_dopfreq = dct_sp_result["n_dopfreq"]
    range_km = 1.e-3 * dct_sp_result["range_m"]

    if range_index is None:
        range_index = int(n_range / 2)

    # get plot filename
    time_str = datetime.datetime.utcfromtimestamp(
        dct_sp_result["utc_unixtimestamp"]).strftime('%y-%m-%dT%H-%M-%S.%fZ')
    plt_folder, plt_filename = \
        radar_filenames.get_filename(
            root_folder=root_folder,
            folder_tag=folder_tag,
            file_tag=file_tag,
            radar_tag=dct_sp_result["instrument_name"],
            utc_fmt_file=time_str,
            end_tag=plot_variable,
            filename_ext="png")

    dct_plot_rd_var = get_dct_plot_rd_var(
        dct_sp_result, "range"+plot_variable)
    if dct_plot_rd_var is None:
        return

    noise_level_dB = None
    clipping_threshold = None
    y_min = None
    y_max = None

    title = dct_plot_rd_var["title"] + " @{:.2f} km".format(
        range_km[range_index])

    # clipping thresholds
    if plot_variable in [
            "doppler_reflectivity_HH",
            "doppler_reflectivity_VV"]:
        my_pol = plot_variable[-2:]
        noise_level_dB = dB(n_dopfreq * dct_sp_result["noise_level_lin_"+my_pol][range_index])
        clipping_threshold = dB(
            n_dopfreq * dct_sp_result["noise_clipping_level_lin_"+my_pol][range_index])

    if plot_variable == "doppler_copolar_correlation":
        if "copolar_correlation_adaptive_threshold" in dct_sp_result["moments"]:
            clipping_threshold = \
                dct_sp_result["moments"]["copolar_correlation_adaptive_threshold"][range_index]
        if "copolar_correlation_filter_threshold" in dct_sp_result["moments"]:
            clipping_threshold = \
                dct_sp_result["moments"]["copolar_correlation_filter_threshold"][range_index]

    if plot_variable == "doppler_sLDR_dB_HV":
        if "sLDR_HV_filter_threshold_db" in dct_sp_settings:
            clipping_threshold = dct_sp_settings["sLDR_HV_filter_threshold_db"]
    if plot_variable == "doppler_sLDR_dB_VH":
        if "sLDR_VH_filter_threshold_db" in dct_sp_settings:
            clipping_threshold = dct_sp_settings["sLDR_VH_filter_threshold_db"]
    if plot_variable == "doppler_sZDR_dB":
        if "abs_sZDR_filter_threshold_db" in dct_sp_settings:
            clipping_threshold = [
                -dct_sp_settings["abs_sZDR_filter_threshold_db"],
                dct_sp_settings["abs_sZDR_filter_threshold_db"]]

    y_data = dct_plot_rd_var["spectrum_data"][:, range_index]
    x_data = np.linspace(dct_plot_rd_var["spec_vmin"], dct_plot_rd_var["spec_vmax"], len(y_data), endpoint=True)

    fig = plt.figure(figsize=(10, 6))
    ax_plt = plt.subplot2grid((1, 8), (0, 0), colspan=5)
    ax_txt = plt.subplot2grid((1, 8), (0, 5), colspan=3)

    ax_plt.plot(x_data, y_data)

    make_legend = False

    if noise_level_dB is not None:
        ax_plt.hlines(
            noise_level_dB,
            xmin=x_data[0], xmax=x_data[-1],
            label="Noise level")
        make_legend = True

    if clipping_threshold is not None:
        ax_plt.hlines(
            clipping_threshold,
            xmin=x_data[0], xmax=x_data[-1],
            linestyle='dashed',
            label="Clipping threshold")
        make_legend = True

    if make_legend:
        ax_plt.legend()

    ax_plt.set_xlabel("Doppler velocity [m s$^{-1}$]")
    ax_plt.set_ylabel(dct_plot_rd_var["cbar_lab"])
    ax_plt.set_title(title)

    details_txt = (
        dct_sp_result["settings_overview_str"] +
        dct_sp_result["mcl1_data_overview_str"])
    ax_txt.text(
        0.0, 0.92, details_txt, fontsize=5,
        horizontalalignment='left', verticalalignment='top',
        transform=ax_txt.transAxes)

    ax_txt.axis('off')
    plt.tight_layout()

    if verbose:
        print("Saving to {} .".format(plt_filename))
    os.makedirs(
        plt_folder,
        exist_ok=True)
    plt.savefig(plt_filename, dpi=300)


def plot_all_doppler(
        dct_sp_result,
        dct_sp_settings,
        verbose=True):
    """Plot all doppler spectra."""
    global all_rd_keys

    if verbose:
        print("plotting all doppler spectra.")

    for key in all_rd_keys:
        # remove range
        new_key = key[5:]
        plot_doppler(
            dct_sp_result,
            dct_sp_settings,
            new_key)


def plot_spectrum(
        dct_sp_result,
        plot_variable="rangedoppler_reflectivity_HH",
        root_folder=skyrapy_plot_dir,
        folder_tag="level1_quicklooks",
        file_tag="level1_quicklooks",
        verbose=True):
    """Plot spectrum."""

    if verbose:
        print("Plotting {}".format(plot_variable))

    # get plot filename
    time_str = datetime.datetime.utcfromtimestamp(
        dct_sp_result["utc_unixtimestamp"]).strftime('%y-%m-%dT%H-%M-%S.%fZ')
    plt_folder, plt_filename = \
        radar_filenames.get_filename(
            root_folder=root_folder,
            folder_tag=folder_tag,
            file_tag=file_tag,
            radar_tag=dct_sp_result["instrument_name"],
            utc_fmt_file=time_str,
            end_tag=plot_variable,
            filename_ext="png")

    dct_plot_rd_var = get_dct_plot_rd_var(
        dct_sp_result, plot_variable)

    if dct_plot_rd_var is None:
        return

    my_extent = [
        dct_plot_rd_var["spec_vmin"],
        dct_plot_rd_var["spec_vmax"],
        dct_plot_rd_var["range_km_min"],
        dct_plot_rd_var["range_km_max"],]

    fig = plt.figure(figsize=(10, 6))
    ax_plt = plt.subplot2grid((1, 8), (0, 0), colspan=5)
    ax_txt = plt.subplot2grid((1, 8), (0, 5), colspan=3)

    my_norm = matplotlib.colors.Normalize(
        vmin=dct_plot_rd_var["my_val_min"],
        vmax=dct_plot_rd_var["my_val_max"])
    my_colors = dct_plot_rd_var["my_cmap"](my_norm(np.transpose(dct_plot_rd_var["spectrum_data"])))

    im = ax_plt.imshow(
        my_colors,
        origin="lower",
        cmap=dct_plot_rd_var["my_cmap"],
        extent=my_extent,)

    ax_plt.vlines(
        dct_plot_rd_var["vmax_lines"],
        ymin=dct_plot_rd_var["range_km_min"],
        ymax=dct_plot_rd_var["range_km_max"],
        colors="white",
        alpha=0.5)

    divider = make_axes_locatable(ax_plt)
    cax = divider.append_axes(
        'right', size='5%', pad=0.1, axes_class=plt.Axes)
    cbar = matplotlib.colorbar.ColorbarBase(
        ax=cax,
        cmap=dct_plot_rd_var["my_cmap"],
        norm=my_norm,
        extend="both",
        )

    cbar.set_label(dct_plot_rd_var["cbar_lab"])

    if dct_plot_rd_var["my_MaxNLocator"] is not None:
        cbar.locator = matplotlib.ticker.MaxNLocator(nbins=dct_plot_rd_var["my_MaxNLocator"])
        cbar.update_ticks()

    ax_plt.set_xlabel("Doppler velocity [m s$^{-1}$]")
    ax_plt.set_ylabel("Range [km]")
    ax_plt.set_title(dct_plot_rd_var["title"])

    details_txt = (
        dct_sp_result["settings_overview_str"] +
        dct_sp_result["mcl1_data_overview_str"])
    ax_txt.text(
        0.0, 0.92, details_txt, fontsize=5,
        horizontalalignment='left', verticalalignment='top',
        transform=ax_txt.transAxes)

    ax_txt.axis('off')
    plt.tight_layout()

    if verbose:
        print("Saving to {} .".format(plt_filename))
    os.makedirs(
        plt_folder,
        exist_ok=True)
    plt.savefig(plt_filename, dpi=300)


def plot_all_spectra(
        dct_sp_result,
        verbose=True):
    """Plot all rangedoppler spectra."""
    global all_rd_keys

    if verbose:
        print("plotting all rangedoppler spectra.")

    for key in all_rd_keys:
        plot_spectrum(
            dct_sp_result,
            key)


def plot_moment(
        dct_sp_result,
        plot_variable="reflectivity_dB_HH",
        root_folder=skyrapy_plot_dir,
        folder_tag="level1_quicklooks",
        file_tag="level1_quicklooks",
        verbose=True):
    """Plot radar moments."""
    dct_moments = dct_sp_result["moments"]
    range_m = dct_sp_result["range_m"]

    my_data = None
    if plot_variable in dct_moments:
        my_data = dct_moments[plot_variable]

    if my_data is None:
        print("Variable not found. ->", plot_variable)
        return

    # get plot filename
    time_str = datetime.datetime.utcfromtimestamp(
        dct_sp_result["utc_unixtimestamp"]).strftime('%y-%m-%dT%H-%M-%S.%fZ')
    plt_folder, plt_filename = \
        radar_filenames.get_filename(
            root_folder=root_folder,
            folder_tag=folder_tag,
            file_tag=file_tag,
            radar_tag=dct_sp_result["instrument_name"],
            utc_fmt_file=time_str,
            end_tag=plot_variable,
            filename_ext="png")

    fig = plt.figure(figsize=(10, 6))
    ax_plt = plt.subplot2grid((1, 8), (0, 0), colspan=5)
    ax_txt = plt.subplot2grid((1, 8), (0, 5), colspan=3)

    ax_plt.plot(range_m * 1.e-3, my_data)
    ax_plt.set_xlabel("Range [km]")
    ax_plt.set_ylabel(plot_variable)

    ax_plt.grid(True)

    details_txt = (
        dct_sp_result["settings_overview_str"] +
        dct_sp_result["mcl1_data_overview_str"])
    ax_txt.text(
        0.0, 0.92, details_txt, fontsize=5,
        horizontalalignment='left', verticalalignment='top',
        transform=ax_txt.transAxes)

    ax_txt.axis('off')

    plt.tight_layout()

    if verbose:
        print("Saving to {} .".format(plt_filename))
    os.makedirs(
        plt_folder,
        exist_ok=True)
    plt.savefig(plt_filename, dpi=300)


def plot_all_moments(
        dct_sp_result,
        verbose=True):
    all_keys = dct_sp_result["moments"].keys()

    if verbose:
        print("plotting all moments.")

    for key in all_keys:
        plot_moment(
            dct_sp_result,
            key)

def dB(x):
    """Caculate dB value."""
    return np.where(
        np.isnan(x) | np.isinf(x) | (x < 0.),
        np.nan,
        10. * np.log10(x))

def dB_inv(x):
    """Caculate inverse of dB value."""
    return np.where(
        np.isnan(x) | np.isinf(x),
        np.nan,
        10 ** (x / 10.))

def running_average(x, dn):
    """Calculate running average of a variable."""
    x2 = np.nan_to_num(x)
    is_not_nan = (np.isnan(x) == False) * 1.

    w = np.convolve(is_not_nan, np.ones((dn,)), mode="same")
    r = np.convolve(x2, np.ones((dn,)), mode="same") / w

    return r

def averaging_2D(
        x, kernel_size=5):
    """Apply 2D averaging to x."""

    # make a kernel
    kernel_shape = (kernel_size, kernel_size)
    kernel = np.ones(kernel_shape)

    x_is_finite = np.isfinite(x)
    x_nonans = x
    x_nonans[~x_is_finite] = 0

    # apply kernel to the data
    convolved_data = scipy.signal.convolve2d(
        x_nonans,
        kernel,
        boundary='fill',
        mode='same',
        fillvalue=0.)
    convolved_weights = scipy.signal.convolve2d(
        x_is_finite,
        kernel,
        boundary='fill',
        mode='same',
        fillvalue=0.)
    convolved_weights[convolved_weights == 0.] = 1.

    averaged_data = convolved_data / convolved_weights

    return averaged_data

def basic_2D_speckle_filter(
        filter_valid_data,
        kernel_size=3):
    """Basic 2D speckle filter."""

    # make a kernel
    kernel_shape = (kernel_size, kernel_size)
    kernel = np.ones(kernel_shape)

    # filter step 1: speckle kernel applied
    # - remove small speckle
    # - fills small gaps

    filter_valid_data_2 = skimage.morphology.binary_opening(
        skimage.morphology.binary_closing(
            filter_valid_data, kernel),
        kernel)

    filter_speckle = (
        (filter_valid_data == True) &
        (filter_valid_data_2 == False))

    return filter_speckle


def demonstrate_windows():
    """Demonstrate several spectral filtering windows by plotting them."""
    lst_labs = [
        "Chebyshev",
        "Hamming",
        r"ConfinedGaussian ($\sigma_t$=0.10)",
        r"ConfinedGaussian ($\sigma_t$=0.05)",
        r"ConfinedGaussian ($\sigma_t$=0.02)",
    ]

    fig = plt.figure()

    for my_lab in lst_labs:
        if "ConfinedGaussian" in my_lab:
            window_name = "ConfinedGaussian"
            window_parameter1 = float(my_lab[-5:-1])
        else:
            window_name = my_lab
            window_parameter1 = None

        n = 100
        my_w = get_scaled_window(
            n,
            window_name=window_name,
            window_parameter1=window_parameter1)
        my_w /= np.max(my_w)

        plt.plot(my_w, label=my_lab)

    plt.legend()
    plt.title("Windows")
    plt.tight_layout()
    plt.savefig(skyrapy_plot_dir+"/demonstration_windows.png", dpi=200)


def dev_demonstrate_auto_cc_threshold(dct_sp_result):
    """Demonstrate the auto cc thresholding technique."""
    # plot copolar_correlation_adaptive_threshold
    plot_moment(
        dct_sp_result,
        "copolar_correlation_adaptive_threshold")

    #
    #
    # apply the auto thresholding again, to get all the variables
    my_data_dct = dct_sp_result["data"]
    rd_cop_corr = my_data_dct["rangedoppler_copolar_correlation"]
    n_dopfreq = dct_sp_result["n_dopfreq"]
    n_range = dct_sp_result["n_range"]

    # reorganize the data
        # this reshape puts together
        # data for range index [0:10], [10:20], etc. etc.
    alt_range_step = 25
    alt_n_range = int(n_range / alt_range_step)
    alt_i_range_0 = n_range - (alt_n_range * alt_range_step)
    alt_cc = rd_cop_corr[:, alt_i_range_0:].reshape(
        alt_range_step * n_dopfreq, alt_n_range, order="C")

    # take different variable
    f_theta = lambda cc: np.pi - np.arccos(cc)
    f_cc = lambda theta: np.cos(np.pi - theta)
    alt_theta_cc = f_theta(alt_cc)

    # sort it
    sorted_alt_theta_cc = np.sort(alt_theta_cc, axis=0)

    # make a subset that is assumed to only contains noise
    # use lowest x% of the sorted data
    max_n_ax0 = int(0.30 * sorted_alt_theta_cc.shape[0])
    alt_theta_cc_noise_subset = sorted_alt_theta_cc[:max_n_ax0, :]
        # determine index to  frequency that determines how much data is used

    # step 1: estimate the distribution parameters for noise theta_cc
    # - using truncated normal distribution estimation of parameters
    # c_1, c_2 = 0.9658563337421513, 0.6881815532308918  # (coefficients for 40% data fraction)
    c_1, c_2 = 1.158975380666913, 0.7354566491542899  # (coefficients for 30% data fraction)
    # c_1, c_2 = 1.3998096020390423, 0.7813574379253931  # (coefficients for 20% data fraction)
    
    mom_1 = np.mean(alt_theta_cc_noise_subset, axis=0)
    mom_2 = np.mean(alt_theta_cc_noise_subset ** 2., axis=0)
    noise_theta_cc_std = np.sqrt(
        (mom_2 - (mom_1**2.))/
        (1 - c_2))
    noise_theta_cc_mu = mom_1 + noise_theta_cc_std * c_1

    # step 2: make histograms of the noise distribution
    n_bins = 100
    n_bin_edges = n_bins + 1
    hist_bin_edges = np.linspace(0, np.pi, n_bin_edges)

    hist_theta_cc = np.empty((n_bins, alt_n_range))
    for i_alt_range in range(alt_n_range):
        hist_theta_cc[:, i_alt_range] = np.histogram(
            alt_theta_cc[:, i_alt_range], bins=hist_bin_edges)[0]

    # step 3: calculate theoretical noise distribution
    hist_mod_theta_cc = np.empty((n_bins, alt_n_range))
    for i_alt_range in range(alt_n_range):
        cdf_vals = scipy.stats.norm.cdf(
            hist_bin_edges, loc=noise_theta_cc_mu[i_alt_range],
            scale=noise_theta_cc_std[i_alt_range])
        hist_mod_theta_cc[:, i_alt_range] = np.diff(cdf_vals)

    # step 4: do tests with jensen shanon
    # and estimate cc threshold
    
    # (start with last one)
    for i_alt_range in range(alt_n_range - 1, -1, -1):
        # test with jensen shanon

        lst_dat_1 = np.zeros(n_bins) + np.nan
        lst_dat_2 = np.zeros(n_bins) + np.nan
        for i_1 in range(75, n_bins - 1):
            hist_cc_before = hist_theta_cc[:i_1, i_alt_range]
            hist_cc_after = hist_theta_cc[i_1:, i_alt_range]
            hist_cc_mod_before = hist_mod_theta_cc[:i_1, i_alt_range]
            hist_cc_mod_after = hist_mod_theta_cc[i_1:, i_alt_range]

            test_1 = scipy.spatial.distance.jensenshannon(
                hist_cc_before,
                hist_cc_mod_before)
            test_2 = scipy.spatial.distance.jensenshannon(
                hist_cc_after,
                hist_cc_mod_after)

            lst_dat_1[i_1] = test_1
            lst_dat_2[i_1] = test_2

        lst_dat_3 = np.array(lst_dat_2) - np.array(lst_dat_1)

        i_argmax = np.nanargmax(lst_dat_3)
        my_treshold = hist_bin_edges[i_argmax]
        my_treshold_cc = f_cc(hist_bin_edges[i_argmax])

        #
        #
        # plot stuff
        xdata = hist_bin_edges[:-1]

        fig = plt.figure()

        # ax 1
        ax = plt.subplot(221)
        plt.vlines([my_treshold], ymin=0, ymax=1, label="Threshold")

        dat = hist_mod_theta_cc[:, i_alt_range]
        i_noise_max = np.argmax(dat)

        dat /= dat[i_noise_max]

        my_slice = slice(None, None, None)
        plt.plot(xdata[my_slice], dat[my_slice], label="CC noise model distribution (Gaussian)")

        dat = hist_theta_cc[:, i_alt_range]
        dat = dat[my_slice] / dat[i_noise_max]
        plt.plot(xdata[my_slice], dat, label="CC distribution")

        plt.xlabel(r"$\theta_{cc}$")
        plt.ylabel("relative frequency")

        plt.legend(fontsize=6)

        # ax 3
        ax = plt.subplot(223)

        my_sorted_data = sorted(alt_cc[:, i_alt_range])
        plt.plot(my_sorted_data, label="cc")
        my_n = len(my_sorted_data)
        plt.hlines([my_treshold_cc], xmin=0, xmax=my_n, label="Threshold")

        plt.ylabel("CC")

        # 2
        ax = plt.subplot(222)
        ax.plot(xdata, lst_dat_1, label="JSD_{x<x_T}")
        ax.plot(xdata, lst_dat_2, label="JSD_{x>x_T}")
        plt.vlines([my_treshold], ymin=0, ymax=1, label="Threshold")
        plt.xlabel(r"$\theta_{cc}$")
        plt.ylabel("JSD")
        plt.legend(fontsize=6)

        # 4
        ax = plt.subplot(224)
        ax.plot(xdata, lst_dat_3, label="$Delta$JSD")
        plt.vlines([my_treshold], ymin=0, ymax=1, label="Threshold")
        plt.xlabel(r"$\theta_{cc}$")
        plt.ylabel(r"$\Delta $JSD")
        plt.legend(fontsize=6)

        plt.legend(fontsize=6)
        plt.tight_layout()

        plt.show()
        plt.clf()
        
        


def dev(
        verbose=True):
    """Development stuff. Please ignore it."""
    sl1_filename = \
        "/Users/albertoudenijhuis/Downloads/data/RIJNMOND_rawdata/2020-10-06_RIJNMOND_rawdata_rain/level1_data/level1_data_RIJNMOND_2020-10-06T11-21-19Z_000000001.sl1"
    sl1_noise_filename = \
        "/Users/albertoudenijhuis/Downloads/data/RIJNMOND_rawdata/level1_noise/level1_noise_RIJNMOND_2020-02-04T13-41-25Z_000000001.sl1"
    n_dopfreq = 512

    if sl1_noise_filename is not None:
        if verbose:
            print("Loading noise data from sl1 file.")
        mcl1_noise = level1_io.load_mcl1_data_from_sl1(
            sl1_noise_filename,
            n_dopfreq=10*n_dopfreq,
            time_index=0)
    else:
        mcl1_noise = None

    if sl1_filename is not None:
        if verbose:
            print("Loading data from sl1 file.")
        mcl1_data = level1_io.load_mcl1_data_from_sl1(
            sl1_filename,
            n_dopfreq=n_dopfreq,
            time_index=0)
    else:
        mcl1_data = None

    if verbose:
        print("Starting spectral processing")

    # change some of the default settings
    dct_sp_settings_dev = {
        # ~ "doppler_dealiasing_use_differential_phase": True,
    }

    dct_sp_result, dct_sp_settings = spectral_processing_v2021(
        mcl1_noise=mcl1_noise,
        mcl1_data=mcl1_data,
        dct_sp_settings_in=dct_sp_settings_dev)

    # ~ plot_all_spectra(dct_sp_result)
    # ~ plot_all_doppler(dct_sp_result, dct_sp_settings)
    # ~ plot_all_moments(dct_sp_result)

    dev_demonstrate_auto_cc_threshold(dct_sp_result)



if __name__ == '__main__':
    verbose = True
    user_action = None
    user_filename = None

    if len(sys.argv) > 1:
        user_action = sys.argv[1]
    if len(sys.argv) > 2:
        user_filename = sys.argv[2]

    if user_action == "dev":
        dev()

    if user_action is None:
        # print documentation
        print(__doc__)
