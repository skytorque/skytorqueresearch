"""
level1_io.py
- A python script from the SkyEcho Radar Python library (skyrapy).
- Read some data from a level1 radar data file. Either NetCDF or sl1.

Copyright 2021 SkyEcho

Update history
- First version (AON, 2020-08-21).
- Fast sl1 reading and some corrections (AON, 2020-10-20).
- Inspection function added (AON, 2021-02-02).
- Simplified loading function added, load_mcl1_data_from_sl1. (AON, 2021-02-02).
- File attributes are added to mcl1-data (as default-enable option). (AON, 2021-02-03).
- Efficient loading for specific time index (AON, 2021-03-04).
- range_m, doppler_velocity, radar_constant_lin_* added to mcl1_data (AON, 2021-03-15)

whish-list:
- Implement it in the spectral processing demo.
- Make option to load mcl1 data for a specific azimuth.
- Make possibility for data inspection without loading the data.
  (I.e. to find total number of sweeps in the data).

Usage examples:
1. Inspect a sl1 file from the command line.
$ python level1_io.py inspect level1_data_RIJNMOND_2020-03-06T09-03-04Z.sl1

2. Load mcl1 data from a sl1 file using Python.
$ import level1_io
$ my_sl1_filename = "/../path_to_sl1_file/../level1_data_RIJNMOND_2020-09-23T18-33-14Z_000000001.sl1"
$ mcl1_data_0 = level1_io.load_mcl1_data_from_sl1(my_sl1_filename, time_index=0)
$ mcl1_data_1 = level1_io.load_mcl1_data_from_sl1(my_sl1_filename, time_index=1)

2. Load mcl1 data from a sl1 file with the reader object using Python.
$ import level1_io
$ my_sl1_filename = "/../path_to_sl1_file/../level1_data_RIJNMOND_2020-09-23T18-33-14Z_000000001.sl1"
$ my_mcl1_reader = level1_io.mcl1_reader(my_sl1_filename)
$ mcl1_data_0 = my_mcl1_reader.get_mcl1_data()
$ mcl1_data_1 = my_mcl1_reader.get_mcl1_data()
"""

import sys
import os
import copy
import pprint
import struct
from collections import deque

import array
import numpy as np
import netCDF4 as nc

dFillValue = -999.9
fFillValue = np.float32(-999.9)

class mcl1_reader:
    """Class to support reading and writing of multi channel level1 radar voltage data."""
    def __init__(
            self,
            filename,
            verbose=True):
        """Initialize the mcl1 reader object."""
        self.ok = True
        self.verbose = verbose
        self.filename = filename

        self.open_file()
        self.buffer_create()


    def open_file(
            self):
        """Open a file."""
        if not self.ok:
            return

        if self.verbose:
            print("Opening {} .".format(self.filename))

        if not os.path.exists(self.filename):
            if self.verbose:
                print("File does not exists.")
            self.ok = False
            return

        self.file_extension = os.path.splitext(self.filename)[1]
        extension_recognized = False

        if self.file_extension == ".nc":
            extension_recognized = True
            result = self.nc_open_file()
            if not result:
                print("Error while reading the NetCDF file.")
                self.ok = False
                return

        if self.file_extension == ".sl1":
            extension_recognized = True
            result = self.sl1_open_file()
            if not result:
                print("Error while reading the sl1 file.")
                self.ok = False
                return

        if not extension_recognized:
            if self.verbose:
                print("File extension not recognized.")
            self.ok = False
            return


    def get_mcl1_data(
            self,
            n_dopfreq=512,
            n_dopfreq_to_pop=None,
            add_file_attributes=True):
        """Get mcl1 data."""
        if not self.ok:
            return None
        if n_dopfreq_to_pop is None:
            n_dopfreq_to_pop = n_dopfreq

        # load sufficient data into buffer
        result = self.buffer_load_data(n_dopfreq)
        if not result:
            if self.verbose:
                print("Failed to load sufficient data into buffer.")
            return None

        # copy data from buffer
        utc_unixtimestamp_start = self.buffer["utc_unixtimestamp"][0]
        utc_unixtimestamp_stop = self.buffer["utc_unixtimestamp"][n_dopfreq-1]

        if n_dopfreq == 1:
            utc_unixtimestamp_delta = 0
        else:
            utc_unixtimestamp_delta = ((
                self.buffer["utc_unixtimestamp"][n_dopfreq-1] -
                self.buffer["utc_unixtimestamp"][0]) / (n_dopfreq - 1))
        utc_unixtimestamp = (
            self.buffer["utc_unixtimestamp"][0] +
            (((n_dopfreq - 1.) / 2.) * utc_unixtimestamp_delta))

        # azimuth
        if n_dopfreq == 1:
            antenna_azimuth_angle_delta_rad = 0
        else:
            antenna_azimuth_angle_delta_rad = (angle_rad_diff(
                self.buffer["antenna_azimuth_angle_rad"][n_dopfreq-1],
                self.buffer["antenna_azimuth_angle_rad"][0]) / (n_dopfreq - 1.))
        antenna_azimuth_angle_resolution_rad = np.abs(
            antenna_azimuth_angle_delta_rad * n_dopfreq)
        antenna_azimuth_angle_rad = (
            (self.buffer["antenna_azimuth_angle_rad"][0] +
             (antenna_azimuth_angle_delta_rad * (n_dopfreq - 1.) / 2.))
            % (2 * np.pi))

        # elevation
        if n_dopfreq == 1:
            antenna_elevation_angle_delta_rad = 0
        else:
            antenna_elevation_angle_delta_rad = (angle_rad_diff(
                self.buffer["antenna_elevation_angle_rad"][n_dopfreq-1],
                self.buffer["antenna_elevation_angle_rad"][0]) / (n_dopfreq - 1.))
        antenna_elevation_angle_resolution_rad = np.abs(
            antenna_elevation_angle_delta_rad * n_dopfreq)
        antenna_elevation_angle_rad = (
            (self.buffer["antenna_elevation_angle_rad"][0] +
             (antenna_elevation_angle_delta_rad * (n_dopfreq - 1.) / 2.))
            % (2 * np.pi))

        my_buf = self.buffer["transmit_pow_W"]
        transmit_pow_W = np.average(
            [my_buf[0], my_buf[n_dopfreq-1]])

        my_buf = self.buffer["attenuation_H_dB"]
        attenuation_H_dB = np.average(
            [my_buf[0], my_buf[n_dopfreq-1]])

        my_buf = self.buffer["attenuation_V_dB"]
        attenuation_V_dB = np.average(
            [my_buf[0], my_buf[n_dopfreq-1]])

        my_buf = self.buffer["temperature_1_C"]
        temperature_1_C = np.average(
            [my_buf[0], my_buf[n_dopfreq-1]])

        my_buf = self.buffer["temperature_2_C"]
        temperature_2_C = np.average(
            [my_buf[0], my_buf[n_dopfreq-1]])

        n_samples = self.attributes["n_samples"]
        mcl1_data = {
            "n_samples": self.attributes["n_samples"],
            "n_dopfreq": n_dopfreq,
            "n_tot": n_samples * n_dopfreq,
            "utc_unixtimestamp_start": utc_unixtimestamp_start,
            "utc_unixtimestamp_stop": utc_unixtimestamp_stop,
            "utc_unixtimestamp": utc_unixtimestamp,
            "antenna_azimuth_angle_rad": antenna_azimuth_angle_rad,
            "antenna_elevation_angle_rad": antenna_elevation_angle_rad,
            "antenna_azimuth_angle_resolution_rad": antenna_azimuth_angle_resolution_rad,
            "antenna_elevation_angle_resolution_rad": antenna_elevation_angle_resolution_rad,
            "transmit_pow_W": transmit_pow_W,
            "attenuation_H_dB": attenuation_H_dB,
            "attenuation_V_dB": attenuation_V_dB,
            "temperature_1_C": temperature_1_C,
            "temperature_2_C": temperature_2_C,
        }

        # raw data
        for tag in self.valid_channel_tags:
            my_var = "rd_"+tag
            my_buf = self.buffer[my_var]
            my_data = []

            for i_dopfreq in range(n_dopfreq):
                my_data.append(my_buf[i_dopfreq])

            # flatten (might need some efficiency check)
            mcl1_data["rawdata_linear_"+tag] = np.concatenate(
                my_data)

        # pop data from buffer
        self.buffer_pop_data(n_dopfreq_to_pop)

        # add file attributes to the mcl1 data
        if add_file_attributes:
            mcl1_data.update(copy.deepcopy(self.attributes))

        # add radar constant, range_m, doppler_velocity_ms
        dct_extra_inf = get_dct_extra_inf(
            attributes=self.attributes,
            mcl1_data=mcl1_data,
            n_dopfreq=n_dopfreq)
        mcl1_data.update(dct_extra_inf)

        return mcl1_data


    def skip_data(
            self,
            n_dopfreq=512,):
        """Skip mcl1 data with n_dopfreq."""
        if not self.ok:
            return None

        n_dopfreq_to_skip = n_dopfreq

        # 1. clear buffer
        n_dopfreq_in_buffer = len(self.buffer["utc_unixtimestamp"])
        if n_dopfreq_in_buffer > 0:
            self.buffer_pop_data(n_dopfreq_in_buffer)
            n_dopfreq_to_skip -= n_dopfreq_to_skip

        # 2. skip loading data into the buffer
        data_n_dopfreq = self.attributes["n_dopfreq"]
        n_times_skip_data_loading = int(n_dopfreq_to_skip / data_n_dopfreq)
        if (n_times_skip_data_loading > 0) and (self.file_extension == ".sl1"):
            # do it
            for i in range(n_times_skip_data_loading):
                skip_result = self.sl1_skip_datablock()

                if not skip_result:
                    result_next = self.open_next_sl1_file()

                    if not result_next:
                        # failed to open next sl1 file
                        # (-> data doesn't exists ...)
                        return None

                    skip_result = self.sl1_skip_datablock()

                if not skip_result:
                    # failed to skip)
                    # (-> data doesn't exists ...)
                    return None

            # success
            n_dopfreq_to_skip -= (
                n_times_skip_data_loading * data_n_dopfreq)

        # for NetCDF
        if (n_times_skip_data_loading > 0) and (self.file_extension == ".nc"):
            self.nc_time_index += n_times_skip_data_loading

        # 3. skip remaining n_dopfreq_to_skip
        if n_dopfreq_to_skip > 0:
            # load sufficient data into buffer, to skip remainder
            result = self.buffer_load_data(n_dopfreq_to_skip)
            if not result:
                if self.verbose:
                    print("Failed to load sufficient data into buffer.")
                return None

            # pop data from buffer
            self.buffer_pop_data(n_dopfreq_to_skip)

        return "success"


    def buffer_create(
            self):
        """Create buffer."""
        if not self.ok:
            return
        new_empty_buffer = {
            "utc_unixtimestamp": deque([]),
            "antenna_azimuth_angle_rad": deque([]),
            "antenna_elevation_angle_rad": deque([]),
            "transmit_pow_W": deque([]),
            "attenuation_H_dB": deque([]),
            "attenuation_V_dB": deque([]),
            "temperature_1_C": deque([]),
            "temperature_2_C": deque([]),
        }

        for tag in self.valid_channel_tags:
            new_empty_buffer["rd_"+tag] = deque([])

        self.buffer = new_empty_buffer


    def buffer_load_data(
            self,
            n_dopfreq_required):
        """Load sufficient data into the buffer."""

        while len(self.buffer["utc_unixtimestamp"]) < n_dopfreq_required:
            if self.file_extension == ".nc":
                result = self.nc_load_buffer()
                if not result:
                    # failure
                    return False

            if self.file_extension == ".sl1":
                result = self.sl1_load_datablock_into_buffer()
                if not result:
                    result = self.open_next_sl1_file()
                    if not result:
                        # failed to open next sl1 file
                        return False

                    # if result is true, try in next sequence to load the buffer


        return True


    def buffer_pop_data(
            self,
            n_dopfreq_to_pop=512):
        """Pop data from the buffer."""
        if not self.ok:
            return
        for var in self.buffer:
            my_buf = self.buffer[var]
            [my_buf.popleft() for i in range(n_dopfreq_to_pop)]


    def nc_open_file(
            self):
        """Open a NetCDF4 file with raw radar data."""
        if not self.ok:
            return False

        self.nc_root = nc.Dataset(self.filename, 'r')
        self.nc_vars = self.nc_root.variables.keys()

        # set nc_global_attributes
        self.nc_global_attributes = {}
        for name in self.nc_root.ncattrs():
            self.nc_global_attributes[name] = self.nc_root.getncattr(name)

        # set nc_global_vars
        # - vars that do not have a time dimension
        self.nc_global_vars = {}
        for var in self.nc_vars:
            nc_var = self.nc_root.variables[var]
            if nc_var.dimensions[0] != "time":
                self.nc_global_vars[var] = self.nc_process_var(nc_var[:])

        # set nc_time_index
        # - necessary for loading data into the buffer
        self.nc_time_index = 0

        # set valid channel tags
        # set data_is_complex
        # check if there is data in the file
        self.valid_channel_tags = []

        data_found = False
        for tag in ["HH", "HV", "VH", "VV"]:
            if ((("rawdata_"+tag+"_i") in self.nc_vars) and
                    (("rawdata_"+tag+"_q") in self.nc_vars)):
                self.valid_channel_tags += [tag]
                self.data_is_complex = True
                data_found = True

            if "rawdata_"+tag in self.nc_vars:
                self.valid_channel_tags += [tag]
                self.data_is_complex = False
                data_found = True

        if not data_found:
            return False

        self.nc_load_attributes()

        return True


    def nc_load_attributes(self):
        """Load attributes from a NetCDF4 files."""
        attributes = {
            "n_samples": self.nc_global_attributes["n_samples"],

            "altitude_m": self.nc_global_vars["altitude"][0],
            "latitude_deg": self.nc_global_vars["latitude"][0],
            "longitude_deg": self.nc_global_vars["longitude"][0],

            "bandwidth_MHz": self.nc_global_attributes["bandwidth_MHz"],
            "central_frequency_GHz": self.nc_global_attributes["central_frequency_GHz"],
            "sweep_time_mus": self.nc_global_attributes["sweep_time_mus"],
            "dual_transmission": self.nc_global_attributes["dual_transmission"],

            "instrument_name": self.nc_global_attributes["instrument_name"],
            "institution": self.nc_global_attributes["institution"],
            "references": self.nc_global_attributes["references"],
            "e-mail": self.nc_global_attributes["e-mail"],
            "site_name": self.nc_global_attributes["site_name"],
            "comment": self.nc_global_attributes["comment"],
            "source": self.nc_global_attributes["source"],
        }

        self.attributes = attributes

        # max_abs_value_HH , etc.
        for tag in ["HH", "HV", "VH", "VV"]:
            my_attr = "max_abs_value_" + tag
            if my_attr in self.nc_global_attributes:
                attributes[my_attr] = self.nc_global_attributes[my_attr]
            else:
                attributes[my_attr] = fFillValue

        self.update_attributes()


    def nc_load_buffer(self):
        """Load the buffer from a NetCDF4 file."""
        if not self.ok:
            return False

        # load nc_data for the current time index into the buffer
        # - vars that do have a time dimension
        nc_data = {}
        for var in self.nc_vars:
            nc_var = self.nc_root.variables[var]
            if nc_var.dimensions[0] == "time":
                # check shape
                if self.nc_time_index >= nc_var.shape[0]:
                    return False
                nc_data[var] = self.nc_process_var(nc_var[self.nc_time_index])

        for tag in self.valid_channel_tags:
            rd_buf_name = "rd_"+tag
            nc_rd_complex_i_name = "rawdata_"+tag+"_i"
            nc_rd_complex_q_name = "rawdata_"+tag+"_q"
            nc_rd_name = "rawdata_"+tag

            if ((nc_rd_complex_i_name in self.nc_vars) and
                    (nc_rd_complex_q_name in self.nc_vars)):
                this_rd_data = (
                    np.array(nc_data[nc_rd_complex_i_name]) +
                    1.j * np.array(nc_data[nc_rd_complex_q_name]))

            if nc_rd_name in self.nc_vars:
                # add data to buffer
                this_rd_data = np.array(nc_data[nc_rd_complex_i_name])

            n_points = len(this_rd_data)
            n_dopfreq = n_points / self.n_samples

            my_buf = self.buffer[rd_buf_name]

            for i_dopfreq in range(n_dopfreq):
                my_slice = slice(
                    i_dopfreq * self.n_samples,
                    (i_dopfreq + 1) * self.n_samples)
                my_buf.append(
                    this_rd_data[my_slice])

        # work out time and angles for each sweep
        time_index_min = self.nc_time_index - 1
        time_index_plus = self.nc_time_index + 1
        if time_index_min < 0:
            time_index_min = 0
        if time_index_plus >= self.nc_root.variables["azimuth"].shape[0]:
            time_index_plus = self.nc_time_index
        time_delta_s = (
            self.nc_root.variables["utc_unixtimestamp"][time_index_plus]  -
            self.nc_root.variables["utc_unixtimestamp"][time_index_min])
        az_delta_rad = angle_rad_diff(np.deg2rad(
            self.nc_root.variables["azimuth"][time_index_plus]  -
            self.nc_root.variables["azimuth"][time_index_min]))
        el_delta_rad = angle_rad_diff(np.deg2rad(
            self.nc_root.variables["elevation"][time_index_plus]  -
            self.nc_root.variables["elevation"][time_index_min]))

        time_delta_s /= (time_index_plus - time_index_min)
        az_delta_rad /= (time_index_plus - time_index_min)
        el_delta_rad /= (time_index_plus - time_index_min)

        # might need a correction for discretization
        lst_utc_unixtimestamp = np.linspace(
            nc_data["utc_unixtimestamp"] - time_delta_s/2,
            nc_data["utc_unixtimestamp"] + time_delta_s/2,
            self.nc_global_attributes["n_dopfreq"],
            endpoint=True)
        lst_antenna_azimuth_angle_rad = np.linspace(
            np.deg2rad(nc_data["azimuth"]) - az_delta_rad/2,
            np.deg2rad(nc_data["azimuth"]) + az_delta_rad/2,
            self.nc_global_attributes["n_dopfreq"],
            endpoint=True) % (2 * np.pi)
        lst_antenna_elevation_angle_rad = np.linspace(
            np.deg2rad(nc_data["elevation"]) - el_delta_rad/2,
            np.deg2rad(nc_data["elevation"]) + el_delta_rad/2,
            self.nc_global_attributes["n_dopfreq"],
            endpoint=True) % (2 * np.pi)

        n_dopfreq = self.nc_global_attributes["n_dopfreq"]

        my_buf = self.buffer["utc_unixtimestamp"]
        [my_buf.append(lst_utc_unixtimestamp[_i]) for _i in range(n_dopfreq)]

        my_buf = self.buffer["antenna_azimuth_angle_rad"]
        [my_buf.append(lst_antenna_azimuth_angle_rad[_i]) for _i in range(n_dopfreq)]

        my_buf = self.buffer["antenna_elevation_angle_rad"]
        [my_buf.append(lst_antenna_elevation_angle_rad[_i]) for _i in range(n_dopfreq)]

        transmit_pow_W = nc_data["transmit_pow"] if ("transmit_pow" in nc_data) else dFillValue
        attenuation_H_dB = nc_data["attenuation_H"] if ("attenuation_H" in nc_data) else dFillValue
        attenuation_V_dB = nc_data["attenuation_V"] if ("attenuation_V" in nc_data) else dFillValue
        temperature_1_C = nc_data["temperature_1"] if ("temperature_1" in nc_data) else dFillValue
        temperature_2_C = nc_data["temperature_2"] if ("temperature_2" in nc_data) else dFillValue

        my_buf = self.buffer["transmit_pow_W"]
        [my_buf.append(transmit_pow_W) for _i in range(n_dopfreq)]

        my_buf = self.buffer["attenuation_H_dB"]
        [my_buf.append(attenuation_H_dB) for _i in range(n_dopfreq)]

        my_buf = self.buffer["attenuation_V_dB"]
        [my_buf.append(attenuation_V_dB) for _i in range(n_dopfreq)]

        my_buf = self.buffer["temperature_1_C"]
        [my_buf.append(temperature_1_C) for _i in range(n_dopfreq)]

        my_buf = self.buffer["temperature_2_C"]
        [my_buf.append(temperature_2_C) for _i in range(n_dopfreq)]

        self.nc_time_index += 1
        return True


    def nc_process_var(self, nc_var):
        """Process fill values. And apply offset and scaling factor."""
        if not self.ok:
            return None
        if nc_var is None:
            return None

        if nc_var.dtype in ["|S1", "S32"]:
            return nc_var

        my_FillValues = copy.deepcopy([dFillValue])
        my_add_offset = 0.
        my_scale_factor = 1.

        # assuming that nc_var is a netcdf variable
        if hasattr(nc_var, 'ncattrs'):
            my_ncattrs = nc_var.ncattrs()
            if "_FillValue" in my_ncattrs:
                my_FillValues += [nc_var.getncattr("_FillValue")]
            if "add_offset" in my_ncattrs:
                my_add_offset = nc_var.getncattr("add_offset")
            if "scale_factor" in my_ncattrs:
                my_scale_factor = nc_var.getncattr("scale_factor")

        # process fill values
        isfillval = np.zeros(np.shape(nc_var))
        for my_fill_value in my_FillValues:
            isfillval = isfillval + (np.abs(np.array(nc_var) - my_fill_value) < 0.1)

        res = np.where(
            isfillval > 0,
            np.nan,
            my_scale_factor * np.array(nc_var) + my_add_offset)
        return res


    def sl1_open_file(
            self):
        """Open a sl1 file with raw radar data."""
        if not self.ok:
            return False

        self.sl1_f = open(self.filename, "rb+")
        result = self.sl1_read_file_header()
        if not result:
            return False

        return True


    def open_next_sl1_file(self):
        """Open the next sl1 file with raw radar data."""
        if not self.ok:
            return False

        if hasattr(self, "sl1_f"):
            self.sl1_f.close()
            del self.sl1_f

        # synthesize new filename
        old_filename = self.filename
        str_num = old_filename[-13:-4]
        num = int(str_num)
        new_num = num + 1
        new_filename = (
            old_filename[:-13] +
            "{:0>9}".format(new_num) +
            old_filename[-4:])

        if not os.path.exists(new_filename):
            return False

        # update the filename
        if os.path.exists(new_filename):
            self.filename = new_filename

        # try to open it.
        return self.sl1_open_file()


    def sl1_read_file_header(self):
        """Read an sl1 file header."""

        attributes = {}

        # "sl1" file identifier
        binary_word = self.sl1_f.read(3)
        sl1_file_identifier = binary_word
        if sl1_file_identifier != b"sbf":
            if self.verbose:
                print("Failed file identifier check.")
                print(binary_word)
                print(b"sbf")
            return False

        # - version_number_minor
        # - version_number_major
        # - header length
        binary_word = self.sl1_f.read(12)
        version_number_major, version_number_minor, header_length = \
            struct.unpack('iii', binary_word)
        if not ((version_number_major == 1) and (version_number_minor == 0)):
            if self.verbose:
                print("Failed sl1 version check.")
                print("version_number_major:", version_number_major)
                print("version_number_minor:", version_number_minor)
                print("header_length:", header_length)
            return False

        # n_samples
        # n_dopfreq
        binary_word = self.sl1_f.read(4*2)
        attributes["n_samples"], attributes["n_dopfreq"] = struct.unpack('ii', binary_word)

        # valid_channel_tags
        # data_is_complex
        binary_word = self.sl1_f.read(5)
        lst_bools = struct.unpack('?'*5, binary_word)
        valid_channel_tags = []
        if lst_bools[0]:
            valid_channel_tags += ["HH"]
        if lst_bools[1]:
            valid_channel_tags += ["HV"]
        if lst_bools[2]:
            valid_channel_tags += ["VH"]
        if lst_bools[3]:
            valid_channel_tags += ["VV"]
        if lst_bools[4]:
            self.data_is_complex = True
        else:
            self.data_is_complex = False
        self.valid_channel_tags = valid_channel_tags

        # radar_constant_factor_dB
        # calibration_offset_dB_HH
        # calibration_offset_dB_HV
        # calibration_offset_dB_VH
        # calibration_offset_dB_VV
        binary_word = self.sl1_f.read(8*4)
        lst_doubles = struct.unpack('f'*8, binary_word)
        attributes["radar_constant_factor_dB_HH"] = lst_doubles[0]
        attributes["radar_constant_factor_dB_HV"] = lst_doubles[1]
        attributes["radar_constant_factor_dB_VH"] = lst_doubles[2]
        attributes["radar_constant_factor_dB_VV"] = lst_doubles[3]
        attributes["calibration_offset_dB_HH"] = lst_doubles[4]
        attributes["calibration_offset_dB_HV"] = lst_doubles[5]
        attributes["calibration_offset_dB_VH"] = lst_doubles[6]
        attributes["calibration_offset_dB_VV"] = lst_doubles[7]

        # max abs value
        binary_word = self.sl1_f.read(4*4)
        lst_max_abs_value = np.float32(struct.unpack('f'*4, binary_word))
        attributes["max_abs_value_HH"] = lst_max_abs_value[0]
        attributes["max_abs_value_HV"] = lst_max_abs_value[1]
        attributes["max_abs_value_VH"] = lst_max_abs_value[2]
        attributes["max_abs_value_VV"] = lst_max_abs_value[3]

        # sampling_frequency_MHz
        binary_word = self.sl1_f.read(4)
        attributes["sampling_frequency_MHz"], = struct.unpack('f', binary_word)

        # antenna_mode
        binary_word = self.sl1_f.read(4)
        attributes["antenna_mode"], = struct.unpack('i', binary_word)

        # bandwidth_MHz
        # central_frequency_GHz
        # sweep_time_mus
        # dual_transmission
        binary_word = self.sl1_f.read(4*3)
        (attributes["bandwidth_MHz"],
         attributes["central_frequency_GHz"],
         attributes["sweep_time_mus"]) = struct.unpack(
             'f'*3, binary_word)
        binary_word = self.sl1_f.read(1)
        attributes["dual_transmission"], = struct.unpack('?', binary_word)

        # altitude_m
        binary_word = self.sl1_f.read(4)
        attributes["altitude_m"], = struct.unpack('f', binary_word)

        # latitude_deg
        # longitude_deg
        binary_word = self.sl1_f.read(8*2)
        (attributes["latitude_deg"],
         attributes["longitude_deg"]) = struct.unpack(
             'dd', binary_word)

        # etc
        binary_word = self.sl1_f.read(100*7)
        (instrument_name,
         institution,
         references,
         e_mail,
         site_name,
         comment,
         source) = struct.unpack('100s'*7, binary_word)
        attributes["instrument_name"] = instrument_name.decode('utf-8').strip("\0")
        attributes["institution"] = institution.decode('utf-8').strip("\0")
        attributes["references"] = references.decode('utf-8').strip("\0")
        attributes["e-mail"] = e_mail.decode('utf-8').strip("\0")
        attributes["site_name"] = site_name.decode('utf-8').strip("\0")
        attributes["comment"] = comment.decode('utf-8').strip("\0")
        attributes["source"] = source.decode('utf-8').strip("\0")

        end_identifier = self.sl1_f.read(1)
        if end_identifier != b'\x03':
            if self.verbose:
                print("Failed header end identifier check.")
            return False

        # header length check
        if header_length != self.sl1_f.tell():
            if self.verbose:
                print("Failed header length check.")
                print("header_length: ", header_length)
                print("self.sl1_f.tell()", self.sl1_f.tell())
            return False

        self.attributes = attributes
        self.n_samples = self.attributes["n_samples"]

        return True


    def sl1_create_file_header(self):
        """Make sl1 file header."""

        # create empty file header
        sl1_file_header = b""

        # "sl1" file identifier
        sl1_file_identifier = b"sbf"
        sl1_file_header += sl1_file_identifier

        # - version_number_minor
        # - version_number_major
        # - header length
        version_number_major = 1
        version_number_minor = 0
        header_length = 0  # will be set later
        sl1_file_header += struct.pack(
            'iii',
            version_number_major,
            version_number_minor,
            header_length)

        # n_samples
        # n_dopfreq

        if "n_dopfreq" not in self.attributes:
            self.attributes["n_dopfreq"] = 512

        sl1_file_header += struct.pack(
            'ii',
            self.attributes["n_samples"],
            self.attributes["n_dopfreq"])

        # is there data for HH, HV, VH, VV
        for tag in ["HH", "HV", "VH", "VV"]:
            if tag in self.valid_channel_tags:
                is_there_data_for_this_tag = True
            else:
                is_there_data_for_this_tag = False
            sl1_file_header += struct.pack(
                '?', is_there_data_for_this_tag)

        # complex data ?
        sl1_file_header += struct.pack(
            '?', self.data_is_complex)

        # radar_constant_factor_dB_ ...
        for tag in ["HH", "HV", "VH", "VV"]:
            value = self.attributes["radar_constant_factor_dB_" + tag]
            sl1_file_header += struct.pack('f', value)

        # calibration_offset_dB_ ...
        for tag in ["HH", "HV", "VH", "VV"]:
            value = self.attributes["calibration_offset_dB_" + tag]
            sl1_file_header += struct.pack('f', value)

        # max_abs_value_ ...
        for tag in ["HH", "HV", "VH", "VV"]:
            value = self.attributes["max_abs_value_" + tag]
            sl1_file_header += struct.pack('f', value)

        # sampling_frequency_MHz
        if "sampling_frequency_MHz" not in self.attributes:
            self.attributes["sampling_frequency_MHz"] = 10
        sl1_file_header += struct.pack('f', self.attributes["sampling_frequency_MHz"])

        # antenna_mode
        if "antenna_mode" not in self.attributes:
            self.attributes["antenna_mode"] = 0
        sl1_file_header += struct.pack('i', self.attributes["antenna_mode"])

        # bandwidth_MHz
        # central_frequency_GHz
        # sweep_time_mus
        # dual_transmission
        sl1_file_header += struct.pack(
            'f'*3,
            self.attributes["bandwidth_MHz"],
            self.attributes["central_frequency_GHz"],
            self.attributes["sweep_time_mus"])
        sl1_file_header += struct.pack(
            '?', self.attributes["dual_transmission"])

        # altitude_m
        sl1_file_header += struct.pack(
            'f', self.attributes["altitude_m"])

        # latitude_deg
        # longitude_deg
        sl1_file_header += struct.pack(
            'dd',
            self.attributes["latitude_deg"],
            self.attributes["longitude_deg"])

        # etc
        sl1_file_header += struct.pack(
            '100s'*7,
            self.attributes["instrument_name"].encode('utf-8'),
            self.attributes["institution"].encode('utf-8'),
            self.attributes["references"].encode('utf-8'),
            self.attributes["e-mail"].encode('utf-8'),
            self.attributes["site_name"].encode('utf-8'),
            self.attributes["comment"].encode('utf-8'),
            self.attributes["source"].encode('utf-8'))

        sl1_file_header += b'\x03' # end

        # set the length
        header_length = len(sl1_file_header)
        binary_word = struct.pack(
            'i',
            header_length)
        sl1_file_header = bytearray(sl1_file_header)
        offset = 3 + 8
        for i in range(4):
            sl1_file_header[offset+i] = binary_word[i]

        return sl1_file_header


    def sl1_write_file_header(self, fp):
        """Write sl1 file header."""
        fp.write(self.sl1_create_file_header())


    def sl1_read_block_header(
            self):
        """Read sl1 block header."""
        block_header_length = 3 + 8 + 7*4 + 1

        if not hasattr(self, "sl1_f"):
            return None

        binary_word = self.sl1_f.read(block_header_length)

        if len(binary_word) != block_header_length:
            # failed to read it from the fp
            return None

        if binary_word[:3] != b"dbh":
            if self.verbose:
                print(binary_word[:3])
                print("invalid block header start identifier")
            return None

        if binary_word[block_header_length-1:block_header_length] != b"\x03":
            if self.verbose:
                print("invalid block header end identifier")
                print(binary_word[block_header_length-1])
            return None

        sl1_block_header = {}

        # data variables
        sl1_block_header["utc_unixtimestamp"], = struct.unpack(
            'd', binary_word[3:3+8])
        (sl1_block_header["antenna_azimuth_angle_rad"],
         sl1_block_header["antenna_elevation_angle_rad"],
         sl1_block_header["transmit_pow_W"],
         sl1_block_header["attenuation_H_dB"],
         sl1_block_header["attenuation_V_dB"],
         sl1_block_header["temperature_1_C"],
         sl1_block_header["temperature_2_C"]) = struct.unpack(
             'f'*7, binary_word[3+8:block_header_length-1])

        # header details
        sl1_block_header["n_bytes_block_header"] = (
            3 + 8 + 7 * 4 + 1)

        if self.data_is_complex:
            sl1_block_header["n_bytes_block_data"] = (
                2 * 4 * self.attributes["n_dopfreq"] *
                self.n_samples * len(self.valid_channel_tags))
        else:
            sl1_block_header["n_bytes_block_data"] = (
                4 * self.attributes["n_dopfreq"] *
                self.n_samples * len(self.valid_channel_tags))

        return sl1_block_header


    def sl1_write_block_header(
            self,
            fp,
            sl1_block_header):
        """Write sl1 data block header."""
        binary_word = b"dbh"
        binary_word += struct.pack(
            'd', sl1_block_header["utc_unixtimestamp"])
        binary_word += struct.pack(
            'f'*7,
            sl1_block_header["antenna_azimuth_angle_rad"],
            sl1_block_header["antenna_elevation_angle_rad"],
            sl1_block_header["transmit_pow_W"],
            sl1_block_header["attenuation_H_dB"],
            sl1_block_header["attenuation_V_dB"],
            sl1_block_header["temperature_1_C"],
            sl1_block_header["temperature_2_C"])
        binary_word += b"\x03"
        fp.write(binary_word)


    def sl1_write_datablock(
            self,
            out_fp,
            n_dopfreq_to_pop=None):
        """Read mcl1_data from a source and write it as a sl1 block."""

        # by definition
        # the sl1 block is 512 sweeps
        if "n_dopfreq" not in self.attributes:
            self.attributes["n_dopfreq"] = 512

        # set n_dopfreq_to_pop
        if n_dopfreq_to_pop is None:
            n_dopfreq_to_pop = self.attributes["n_dopfreq"]

        # get mcl1 data
        mcl1_data = self.get_mcl1_data(
            self.attributes["n_dopfreq"],
            n_dopfreq_to_pop)

        if mcl1_data is None:
            return False

        # write datablock header
        # - attribute keys in mcl1_data
        # - are the same as in sl1_block_header
        self.sl1_write_block_header(
            out_fp,
            mcl1_data)

        # write raw data block
        for tag in self.valid_channel_tags:
            if self.data_is_complex:
                binary_word = np.array(
                    np.real(mcl1_data["rawdata_linear_"+tag]), dtype="float32").tobytes()
                out_fp.write(binary_word)
                binary_word = np.array(
                    np.imag(mcl1_data["rawdata_linear_"+tag]), dtype="float32").tobytes()
                out_fp.write(binary_word)

            else:
                binary_word = np.array(
                    np.real(mcl1_data["rawdata_linear_"+tag]), dtype="float32").tobytes()
                out_fp.write(binary_word)

        return True


    def sl1_write_to_file(
            self,
            sl1_output_filename=None,
            n_max_datablocks=None):
        """Write all data to sl1 file."""
        if sl1_output_filename is None:
            file_noext = os.path.splitext(self.filename)[1]
            sl1_output_filename = file_noext + ".sl1"

        if os.path.exists(sl1_output_filename):
            if self.verbose:
                print("File already exists.")
            return False

        # write sl1 binary file
        sl1_fp_output = open(sl1_output_filename, "wb+")
        self.sl1_write_file_header(sl1_fp_output)
        i_datablock = -1
        while True:
            i_datablock += 1

            if n_max_datablocks is not None:
                if i_datablock >= n_max_datablocks:
                    break

            result = self.sl1_write_datablock(sl1_fp_output)
            if not result:
                break

        sl1_fp_output.close()
        return True


    def sl1_load_datablock_into_buffer(self):
        """Load a sl1 datablock into the buffer."""
        if not self.ok:
            return False

        if "n_dop_freq" not in self.attributes:
            self.attributes["n_dopfreq"] = 512

        # load sl1 data for the current time index into the buffer
        # - vars that do have a time dimension

        sl1_block_header = self.sl1_read_block_header()
        if sl1_block_header is None:
            return False

        n_bytes_block_header = sl1_block_header["n_bytes_block_header"]
        n_bytes_block_data = sl1_block_header["n_bytes_block_data"]

        fp_offset_current_blockdata = self.sl1_f.tell()
        fp_offset_block_header_min = (
            fp_offset_current_blockdata - n_bytes_block_header -
            n_bytes_block_data - n_bytes_block_header)
        fp_offset_block_header_plus = fp_offset_current_blockdata + n_bytes_block_data

        if fp_offset_block_header_min > 0:
            self.sl1_f.seek(fp_offset_block_header_min)
            sl1_block_header_min = self.sl1_read_block_header()
        else:
            sl1_block_header_min = None

        self.sl1_f.seek(fp_offset_block_header_plus)
        sl1_block_header_plus = self.sl1_read_block_header()

        # set file pointer back to the coming data block
        self.sl1_f.seek(fp_offset_current_blockdata)

        index_delta = 0
        if sl1_block_header_plus is not None:
            r_block_header_plus = sl1_block_header_plus
            index_delta += 1
        else:
            r_block_header_plus = sl1_block_header

        if sl1_block_header_min is not None:
            r_block_header_min = sl1_block_header_min
            index_delta += 1
        else:
            r_block_header_min = sl1_block_header

        # work out time and angles with high precision
        time_delta_s = (
            r_block_header_plus["utc_unixtimestamp"]  -
            r_block_header_min["utc_unixtimestamp"])
        az_delta_rad = angle_rad_diff(
            r_block_header_plus["antenna_azimuth_angle_rad"]  -
            r_block_header_min["antenna_azimuth_angle_rad"])
        el_delta_rad = angle_rad_diff(
            r_block_header_plus["antenna_elevation_angle_rad"]  -
            r_block_header_min["antenna_elevation_angle_rad"])
        time_delta_s /= index_delta
        az_delta_rad /= index_delta
        el_delta_rad /= index_delta

        # might need a correction for discretization
        lst_utc_unixtimestamp = np.linspace(
            sl1_block_header["utc_unixtimestamp"] - time_delta_s/2,
            sl1_block_header["utc_unixtimestamp"] + time_delta_s/2,
            self.attributes["n_dopfreq"],
            endpoint=True)
        lst_antenna_azimuth_angle_rad = np.linspace(
            sl1_block_header["antenna_azimuth_angle_rad"] - az_delta_rad/2,
            sl1_block_header["antenna_azimuth_angle_rad"] + az_delta_rad/2,
            self.attributes["n_dopfreq"],
            endpoint=True) % (2 * np.pi)
        lst_antenna_elevation_angle_rad = np.linspace(
            sl1_block_header["antenna_elevation_angle_rad"] - el_delta_rad/2,
            sl1_block_header["antenna_elevation_angle_rad"] + el_delta_rad/2,
            self.attributes["n_dopfreq"],
            endpoint=True) % (2 * np.pi)

        my_buf = self.buffer["utc_unixtimestamp"]
        [my_buf.append(lst_utc_unixtimestamp[_i])
         for _i in range(self.attributes["n_dopfreq"])]

        my_buf = self.buffer["antenna_azimuth_angle_rad"]
        [my_buf.append(lst_antenna_azimuth_angle_rad[_i])
         for _i in range(self.attributes["n_dopfreq"])]

        my_buf = self.buffer["antenna_elevation_angle_rad"]
        [my_buf.append(lst_antenna_elevation_angle_rad[_i])
         for _i in range(self.attributes["n_dopfreq"])]

        my_buf = self.buffer["transmit_pow_W"]
        [my_buf.append(sl1_block_header["transmit_pow_W"])
         for _i in range(self.attributes["n_dopfreq"])]

        my_buf = self.buffer["attenuation_H_dB"]
        [my_buf.append(sl1_block_header["attenuation_H_dB"])
         for _i in range(self.attributes["n_dopfreq"])]

        my_buf = self.buffer["attenuation_V_dB"]
        [my_buf.append(sl1_block_header["attenuation_V_dB"])
         for _i in range(self.attributes["n_dopfreq"])]

        my_buf = self.buffer["temperature_1_C"]
        [my_buf.append(sl1_block_header["temperature_1_C"])
         for _i in range(self.attributes["n_dopfreq"])]

        my_buf = self.buffer["temperature_2_C"]
        [my_buf.append(sl1_block_header["temperature_2_C"])
         for _i in range(self.attributes["n_dopfreq"])]

        # read binary data
        for tag in self.valid_channel_tags:
            n_points = self.attributes["n_dopfreq"] * self.n_samples
            # ~ n_bytes = 4 * n_points

            if self.data_is_complex:
                new_float_array_real = array.array("f")
                new_float_array_complex = array.array("f")
                new_float_array_real.fromfile(self.sl1_f, n_points)
                new_float_array_complex.fromfile(self.sl1_f, n_points)
                my_data = np.array(new_float_array_real) + 1.j * np.array(new_float_array_complex)
            else:
                new_float_array = array.array("f")
                new_float_array.fromfile(self.sl1_f, n_points)
                my_data = np.array(new_float_array)

            my_data = my_data.reshape((self.attributes["n_dopfreq"], self.n_samples))

            # add data to buffer
            rd_buf_name = "rd_"+tag
            my_buf = self.buffer[rd_buf_name]
            for i_dopfreq in range(self.attributes["n_dopfreq"]):
                my_buf.append(my_data[i_dopfreq])

        return True


    def sl1_skip_datablock(self):
        """Load a sl1 datablock into the buffer."""
        if not self.ok:
            return False

        if "n_dop_freq" not in self.attributes:
            self.attributes["n_dopfreq"] = 512

        sl1_block_header = self.sl1_read_block_header()
        if sl1_block_header is None:
            return False

        n_bytes_block_data = sl1_block_header["n_bytes_block_data"]

        # forward the filer pointer w/o reading the data
        fp_offset_current_blockdata = self.sl1_f.tell()
        fp_offset_block_header_plus = fp_offset_current_blockdata + n_bytes_block_data
        self.sl1_f.seek(fp_offset_block_header_plus)

        return True


    def update_attributes(self):
        """Support function. Replace nan attributes with dFillValue."""
        for key, value in self.attributes.items():
            if isinstance(value, (np.floating, float)):
                if np.isnan(value):
                    self.attributes[key] = dFillValue


    def __del__(self):
        """Class gets deleted. Clean up."""
        if hasattr(self, "nc_root"):
            self.nc_root.close()
            del self.nc_root

        if hasattr(self, "sl1_f"):
            self.sl1_f.close()


def angle_rad_avg(lst_ang_rad):
    """Calculate angle average."""
    avg_cos = np.average(np.cos(lst_ang_rad))
    avg_sin = np.average(np.sin(lst_ang_rad))
    return np.arctan2(avg_sin, avg_cos) % (2. * np.pi)


def angle_rad_diff(ang_A_rad, ang_B_rad=0.):
    """Calculate angle difference."""
    ang_C_rad = ang_A_rad - ang_B_rad

    if ang_C_rad < -np.pi:
        ang_C_rad += (2 * np.pi)
    if ang_C_rad > np.pi:
        ang_C_rad -= (2 * np.pi)

    return ang_C_rad


def dB(x):
    """Calculate dB value of x."""
    cond_bad = (x == 0.0) | (x == 0)
    cond_bad = cond_bad | (x != x)
    cond_bad = cond_bad | ((2. * x) == x)
    cond_bad = cond_bad | (x == dFillValue)
    cond_bad = cond_bad | (x == fFillValue)
    return np.where(
        cond_bad,
        dFillValue,
        10. * np.log10(x))


def dB_inv(x):
    """Calculate inverse dB value of x."""
    cond_0 = (x == 0.0) | (x == 0)
    cond_bad = (x != x)
    cond_bad = cond_bad | ((2. * x) == x)
    cond_bad = cond_bad | (x == dFillValue)
    cond_bad = cond_bad | (x == fFillValue)

    res = 10. ** (x / 10.)
    res = np.where(cond_0, 1.0, res)
    res = np.where(cond_bad, dFillValue, res)
    return res


def calc_radar_constant(
        radar_constant_factor_dB,
        transmit_pow_W,
        range_m,
        receiver_att_dB=0,
        calibration_offset_dB=0,
        ):
    """Radar constant calculation.
        - Not taking into account fractional sweep usage (due to sweep truncation)."""

    radar_constant_factor_lin = dB_inv(radar_constant_factor_dB)
    receiver_att_lin = dB_inv(receiver_att_dB)
    calibration_offset_lin = dB_inv(calibration_offset_dB)

    crad_part1 = (
        1.e18
        * radar_constant_factor_lin
        * receiver_att_lin
        * calibration_offset_lin
        / transmit_pow_W)

    # multiply with 10^18 to account for unit conversion m^2 m-3 -> mm^6 m-3

    # multipy with r^2.
    radar_constant = crad_part1 * (range_m ** 2.)

    return radar_constant


def get_dct_extra_inf(
        attributes,
        mcl1_data=None,
        n_dopfreq=512):
    """Return dictionary with extra range, doppler and radar constant details."""

    # input parameters
    speed_of_light_ms = 2.99792458e8
    bandwidth_MHz = attributes["bandwidth_MHz"]
    dual_transmission = attributes["dual_transmission"]
    central_frequency_GHz = attributes["central_frequency_GHz"]
    sweep_time_mus = attributes["sweep_time_mus"]
    n_samples = attributes["n_samples"]

    # derived paramters
    n_range = int((n_samples + 1) / 2)
    wavel_m = (
        speed_of_light_ms / (central_frequency_GHz * 1.e9))

    # Calculate range paramters
    range_resolution_m = (
        speed_of_light_ms / (bandwidth_MHz * 1.e6))
    if dual_transmission:
        range_resolution_m /= 2.
    range_m = \
        (np.arange(n_range) + 0.5) * range_resolution_m
    max_range_m = range_m[-1]

    # Calculate doppler velocity parameters
    # - doppler_velocity_max_ms is the maximum Doppler velocity for single pol radar
    # - doppler_velocity_tot_ms is the total range of Doppeler velocities for single pol radar
    #   (2 * vmax, as the spectrum goes from -vmax until +vmax)
    # - for dual pol the maximum Doppler velocity is divided by two.
    #   (this is because the time between consecutive sweeps doubles).
    doppler_velocity_max_ms = \
        speed_of_light_ms / (
            4. * 1.e3 * central_frequency_GHz *
            sweep_time_mus)
    doppler_velocity_tot_ms = 2.0 * doppler_velocity_max_ms
    doppler_velocity_step_ms = doppler_velocity_tot_ms / n_dopfreq
    if dual_transmission:
        doppler_velocity_max_ms /= 2.
        doppler_velocity_tot_ms /= 2.
        doppler_velocity_step_ms /= 2.

    halfDop = int(n_dopfreq / 2)
    doppler_velocity_ms = \
        (np.arange(n_dopfreq) - halfDop) * doppler_velocity_step_ms

    if mcl1_data is not None:
        radar_constant_lin_HH = calc_radar_constant(
            radar_constant_factor_dB=attributes["radar_constant_factor_dB_HH"],
            transmit_pow_W=mcl1_data["transmit_pow_W"],
            range_m=range_m,
            receiver_att_dB=mcl1_data["attenuation_H_dB"],
            calibration_offset_dB=attributes["calibration_offset_dB_HH"],
        )
        radar_constant_lin_HV = calc_radar_constant(
            radar_constant_factor_dB=attributes["radar_constant_factor_dB_HV"],
            transmit_pow_W=mcl1_data["transmit_pow_W"],
            range_m=range_m,
            receiver_att_dB=mcl1_data["attenuation_V_dB"],
            calibration_offset_dB=attributes["calibration_offset_dB_HV"],
        )
        radar_constant_lin_VH = calc_radar_constant(
            radar_constant_factor_dB=attributes["radar_constant_factor_dB_VH"],
            transmit_pow_W=mcl1_data["transmit_pow_W"],
            range_m=range_m,
            receiver_att_dB=mcl1_data["attenuation_H_dB"],
            calibration_offset_dB=attributes["calibration_offset_dB_VH"],
        )
        radar_constant_lin_VV = calc_radar_constant(
            radar_constant_factor_dB=attributes["radar_constant_factor_dB_VV"],
            transmit_pow_W=mcl1_data["transmit_pow_W"],
            range_m=range_m,
            receiver_att_dB=mcl1_data["attenuation_V_dB"],
            calibration_offset_dB=attributes["calibration_offset_dB_VV"],
        )

    dct_extra_inf = {
        "n_range": n_range,
        "wavel_m": wavel_m,

        "range_resolution_m": range_resolution_m,
        "range_m": range_m,
        "max_range_m": max_range_m,

        "doppler_velocity_max_ms": doppler_velocity_max_ms,
        "doppler_velocity_tot_ms": doppler_velocity_tot_ms,
        "doppler_velocity_step_ms": doppler_velocity_step_ms,
        "doppler_velocity_ms": doppler_velocity_ms,

        "radar_constant_lin_HH": radar_constant_lin_HH,
        "radar_constant_lin_HV": radar_constant_lin_HV,
        "radar_constant_lin_VH": radar_constant_lin_VH,
        "radar_constant_lin_VV": radar_constant_lin_VV,
    }

    return dct_extra_inf


def inspect_sl1_file(
        sl1_filename,
        verbose=True):
    """Inspect sl1 file."""

    if not os.path.exists(sl1_filename):
        print("File does not exist.")
        print(" - filename:", sl1_filename)
        return

    # open sl1 file
    my_reader = mcl1_reader(
        sl1_filename)

    # header+data
    print("\n\n")
    print("sl1 file inspection.")
    print(" - filename:", sl1_filename)

    print("\n\n - attributes:")
    pprint.pprint(my_reader.attributes)

    print("\n\n - dct extra inf:")
    dct_extra_inf = get_dct_extra_inf(my_reader.attributes)
    pprint.pprint(dct_extra_inf)

    print("\n\n - first mcl1_data:")
    pprint.pprint(my_reader.attributes)
    mcl1_data = load_mcl1_data_from_sl1(sl1_filename, time_index=0)
    pprint.pprint(mcl1_data)

    return


def load_mcl1_data_from_sl1(
        sl1_filename,
        time_index=0,
        n_dopfreq=512,
        verbose=True):
    """Simplified function to load mcl1 data using indexing."""

    if verbose:
        print("Loading data from sl1 file.")
        print("- filename:", sl1_filename)
        print("- time index:", time_index)

    # open sl1 file
    my_reader = mcl1_reader(
        sl1_filename)

    if time_index != 0:
        n_dopfreq_to_skip = (time_index * n_dopfreq)
        my_reader.skip_data(
            n_dopfreq=n_dopfreq_to_skip)

    mcl1_data = my_reader.get_mcl1_data(
        n_dopfreq=n_dopfreq)
    if mcl1_data is None:
        if verbose:
            print("No data exists for this time index.")
        return None

    if verbose:
        print("Success.")

    return mcl1_data


def dev():
    """Some development stuff. Please ignore it."""
    my_sl1_filename = "/Users/albertoudenijhuis/Downloads/data/RIJNMOND_rawdata/2020-10-06_RIJNMOND_rawdata_rain/level1_data/level1_data_RIJNMOND_2020-10-06T11-21-19Z_000000001.sl1"

    if False:
        print("\n\n\ninspect sl1 file")
        inspect_sl1_file(my_sl1_filename)

    if True:
        # read sl1
        mcl1_data = load_mcl1_data_from_sl1(
            my_sl1_filename, time_index=100, n_dopfreq=512)
        pprint.pprint(mcl1_data)

    if False:
        print("\n\n\ndB")
        x = np.array([0, 10., 100., 200.])
        dB_x = dB(x)
        x_2 = dB_inv(dB_x)
        print("x   :", x)
        print("dB_x:", dB_x)
        print("x_2 :", x_2)
        print()


    if False:
        print("\n\n\nmcl1 attributes")

        # open sl1 file
        my_reader = mcl1_reader(
            my_sl1_filename)
        mcl1_data = my_reader.get_mcl1_data()
        pprint.pprint(mcl1_data)

    # nieuwe functie  open_mcl1
    if False:
        print("\n\n\nmcl1 attributes")

        # one line to get mcl1_data
        my_time_index = 0
        mcl1_data = load_mcl1_data_from_sl1(my_sl1_filename, my_time_index)
        pprint.pprint(mcl1_data)


    if False:
        # read sl1
        # is it quick ??

        # open sl1 file
        my_reader = mcl1_reader(
            my_sl1_filename)

        for i in range(20):
            mcl1_data = my_reader.get_mcl1_data()

        print(mcl1_data)
        print("Done.")

    if False:
        # read sl1 file and write it to another file

        # open sl1 file
        my_reader = mcl1_reader(
            my_sl1_filename)

        if os.path.exists(my_sl1_filename_2):
            os.remove(my_sl1_filename_2)

        if False:
            # write header only
            sl1_fp_output = open(my_sl1_filename_2, "wb")
            my_reader.sl1_write_file_header(sl1_fp_output)


        # header+data
        if True:
            pprint.pprint(my_reader.attributes)

            my_reader.sl1_write_to_file(my_sl1_filename_2)


    if False:
        # read nc and write sl1 header
        my_reader = mcl1_reader(
            my_nc_filename)
        sl1_fp_output = open(my_sl1_headeronly_filename, "wb")
        my_reader.sl1_write_file_header(sl1_fp_output)
        print("p1")
        pprint.pprint(my_reader.attributes)
        del my_reader



    if False:
        # read sl1 header and print attributes
        my_reader = mcl1_reader(
            my_sl1_headeronly_filename_2)
        pprint.pprint(my_reader.attributes)
        del my_reader

    if False:
        # convert nc to sl1
        my_reader = mcl1_reader(
            my_nc_filename)

        my_reader.sl1_write_to_file(
            my_sl1_data_filename,
            n_max_datablocks=3)

    if False:
        # read sl1
        my_reader = mcl1_reader(
            my_sl1_data_filename)
        pprint.pprint(my_reader.attributes)

        mcl1_data = my_reader.get_mcl1_data()


    # ~ pprint.pprint(my_reader.nc_global_vars)
    # ~ mcl1_data = my_reader.get_mcl1_data()
    # ~ pprint.pprint(mcl1_data)

    # ~ del my_reader


if __name__ == '__main__':
    user_action = None
    user_filename = None

    if (len(sys.argv) > 1):
        user_action = sys.argv[1]
    if (len(sys.argv) > 2):
        user_filename = sys.argv[2]

    if (user_action == "inspect") and (len(sys.argv) == 3):
        inspect_sl1_file(user_filename)

    if (user_action == "dev"):
        dev()

    if user_action is None:
        # print documentation
        print(__doc__)
